#!/env/python
#
"""
Export data as json for Constellation Bibliographique D3js app
"""
import json
import os
import marshal

from sledd import *
from sledd_utils import *

chemin_emissions_man = '/home/laurent/Projets/Sledd/bdd/emissions_man'
chemin_donnees = '/home/laurent/Projets/Sledd/bdd'
chemin_articles = '/home/laurent/Projets/Sledd/bdd/articles_manu'

chemin_professions = os.path.join(chemin_donnees, 'professions')

sledd_bis = Sledd(chemin_emissions_man)
sledd_bis.import_from_file(chemin_donnees)
sledd_bis.import_articles(chemin_articles)

professions = marshal.load(open(chemin_professions, 'r'))

def get_auteurs(lst_e):
    """
    recupere la liste des auteurs cites dans la liste d emissions -lst_em-
    """
    ll = [i['numero']-1 for i in lst_e]
    lst_id_aut = []
    [lst_id_aut.extend(i['ids_auteur']) for i in lst_em]
    # for i in sledd_bis.auteurs:
    #     for j in i['ids_emission']:
    #         if j in ll and j not in lst_id_aut:
    #             lst_id_aut.append(i['id_auteur'])
    return lst_id_aut

def get_dic_auteur(aut):
    """
    renvoi un noeud auteur sous forme de dictionnaire
    """
    dd = {}
    dd['id'] = aut['id']
    dd['name'] = aut['nom']
    dd['size'] = len(aut['ids_emission'])
    dd['type'] = 'auteur'
    dd['ids_ref'] = aut['ids_ref']
    b_naissance = False
    if aut['profession'] in professions['scientifiques']:
        dd['group'] = 1
    else:
        dd['group'] = 2
    if aut['naissance']:
        if len(aut['naissance']) == 3:
            dd['naissance'] = aut['naissance'][2]
        else:
            dd['naissance'] = aut['naissance'][0]
        b_naissance = True
    else:
        dd['naissance'] = '?'
    if aut['deces']:
        if len(aut['deces']) == 3:
            dd['deces'] = aut['deces'][2]
        else:
            dd['deces'] = aut['deces'][0]
    else:
        if b_naissance:
            dd['deces'] = ' '
        else:
            dd['deces'] = '?'
    if aut['wikipedia_titre']:
        dd['wiki'] = 'http://%s.wikipedia.org/wiki/%s'%(aut['wikipedia_langue'], aut['wikipedia_titre'].replace(' ', '_'))
        
        
    return dd

def get_dic_em(em):
    """
    renvoi un noeud emission sous forme d un dictionnaire
    """
    dd = {}
    dd['id'] = em['id']
    hash = em['hash'].split('-')
    dd['dateinv'] = int(''.join(hash))
    hash.reverse()
    dd['date'] = '/'.join(hash)
    dd['name'] = em['titre']
    dd['num'] = em['numero']
    dd['size'] = len(em['ids_ref'])
    dd['type'] = 'emission'
    dd['group'] = 0
    dd['lien'] = em['lien_emission']
    dd['ids_ref'] = em['ids_ref']
    return dd

def get_dic_article(art):
    dd = {}
    dd['id'] = art['id']
    dd['name'] = art['titre']
    dd['auteur'] = art['auteurs']
    dd['annee'] = art['annee']
    dd['revue'] = art['revue']
    dd['group'] = 3
    dd['type'] = 'article'
    return dd

def get_nodes(lst_em):
    """
    renvoi la liste de tous les noeuds de la liste d emission -lst_em-
    seuls les auteurs cite sont exportes
    """
    nodes = []
    for i in lst_em:
        nodes.append(get_dic_em(i))

    ll = list(set(get_auteurs(lst_em)))
    for i in ll:
        nodes.append(get_dic_auteur(sledd_bis.auteurs[i]))

    arts = [i for i in sledd_bis.articles if len(i['lst_em'])>1]
    for i in arts:
        nodes.append(get_dic_article(i))
    return nodes

def get_links(lst_em):
    """
    renvoi la liste des liens 
    """
    links = []
    ll = get_auteurs(lst_em)
    l_num = [i['numero']-1 for i in lst_em]
    for i in ll:
        for j in sledd_bis.auteurs[i]['ids_emission']:
            if j in l_num:
                links.append({"source":sledd_bis.auteurs[i]['id'], "target":sledd_bis.emissions[j]['id']})
    links2 = []
    [links2.append(i) for i in links if i not in links2]
    links = links2
    arts = [i for i in sledd_bis.articles if len(i['lst_em'])>1]
    for i in arts:
        for j in i['lst_em']:
            links.append({"source":i['id'], "target":sledd_bis.emissions[j]['id']})

    for i in lst_em[:-1]:
        links.append({"source":i['id'], "target":sledd_bis.emissions[i['numero']]['id']})
    return links

def get_refs():
    """
    renvoi la liste des eferences
    """
    lst_ref = []
    for i in sledd_bis.refs:
        dd = {}
        dd['auteur'] = i['auteur']
        dd['titre'] = i['titre']
        if i.has_key('editeur'): dd['editeur'] = i['editeur']
        if i.has_key('parution'): dd['parution'] = i['parution']
        lst_ref.append(dd)
    return lst_ref
    

lst_em = sledd_bis.emissions

jj = {'nodes':get_nodes(lst_em), 'links':get_links(lst_em), 'refs':get_refs()}

txt = json.dumps(jj, indent = 1)
tt = open('/home/laurent/Projets/ConstellationsBibliographiques/data/sledd2.json', 'w')
tt.write(txt)
tt.close()
