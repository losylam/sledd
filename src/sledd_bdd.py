#!/bin/python
# coding: utf-8

import subprocess
import urllib
import os
import json
import re

from bs4 import BeautifulSoup


from sledd_utils import *

lien_json = "http://clementgrimal.fr/darwin/darwin_base.json"
chemin_json = "/home/laurent/Projets/Sledd/bdd/darwin_base.json"
# chemin_json = "darwin_base.json"
chemin_emissions = '/home/laurent/Projets/Sledd/emissions'
chemin_emissions = '/media/laurent/40b145bf-73e0-4067-84c2-ca8aeed6258c/Sledd/emissions'
chemin_html = '/home/laurent/Projets/Sledd/html'
div_biblio = 'block-fi_emissions_diffusions-bibliographie_diffusion'

def import_json():
    """
    get the json file from clement grimal blog
    """
    try:
        urllib.urlretrieve(lien_json, chemin_json)
    except IOError:
        print 'not connected'
    return json.load(open(chemin_json, 'r'))


def em_is_missing(_json, download=False):
    # liste des hash des emissions telechargees
    l_fichier = [em[:10] for em in os.listdir(chemin_emissions) if em[-4:] == '.mp3']
    for em in _json['emissions']:
        if em['hash'] not in l_fichier:
            print '%s is missing' % em['hash']
            if em['infos'].has_key('lien_mp3') and em['infos']['lien_mp3'] and download:
                print 'downloading...'
                dest_em = os.path.join(chemin_emissions, '%s.mp3' % em['hash'])
                try:
                    urllib.urlretrieve(em['infos']['lien_mp3'], dest_em)
                except IOError:
                    print 'not connected'


def parse_html(html):
    """
    get the book references from an html page
    return a list of dictionnary
    """    
    soup = BeautifulSoup(html)
    biblio = soup.find(id=div_biblio)
    liste = []
    if biblio:
        for dd in biblio.find_all('div'):
            if 'content' in dd.attrs['class']:
                dic_book = {}
                dic_book['titre'] = dd.find_all('h4')[0].get_text()
                aut = dd.find('div', attrs={'class':'description'})
                comp = dd.findAll('div', attrs={'class':'complement'})
                if len(comp)==2:
                    dic_book['editeur'] = comp[0].get_text()[10:]
                    dic_book['parution'] = comp[1].get_text()[-4:]
                if aut: dic_book['auteur'] = aut.get_text()[3:]
                liste.append(dic_book)
    return liste


def parse_html_hash(hash):
    f = open(os.path.join(chemin_html, '%s.html'%hash), 'r')
    t = f.read()
    f.close()
    return parse_html(t)

def parse_html_articles_hash(hash, get_html = True):
    f = open(os.path.join(chemin_html, '%s.html'%hash), 'r')
    t = f.read()
    f.close()
    return parse_html_articles(t, get_html)

def parse_html_articles(html, get_html = True):
    html = sel_apres(html, 'Articles')
    lst_fin = [len(html)]
    if html.find('Agenda') > 0:
        lst_fin.append(html.find('Agenda'))
    if html.find('</div>') > 0:
        lst_fin.append(html.find('</div>'))
    if html.find('Programmation musicale') > 0:
        lst_fin.append(html.find('Programmation musicale'))
    if html.find('itres diffus') > 0:
        lst_fin.append(html.find('itres diffus'))
    if html.find('Livres') > 0:
        lst_fin.append(html.find('Livres'))
    if html.find('Liens') > 0:
        lst_fin.append(html.find('Liens')) 
    lst_fin.sort()
    print lst_fin[0]
    html = html[:lst_fin[0]]
    soup = BeautifulSoup(html)
    l = soup.findAll('p')
    m = soup.findAll('li')
    if len(m) > len(l):
        l = m
    l2 = []
    for ll in l:
        if not ll.text.isspace():
            if get_html:
                l2.append(ll)
            else:
                l2.append(ll.text)
    return l2

def find_article_in_ref(s):
    m = re.search('\s\d{4}[,.]\s', s) # '[{16}{17}{18}{19}{20}]\d{2}[,.]\s'
    if m:
        try:
            s_avant = s[:m.start()]
            s_apres = s[m.end():]
            y = s[m.start():m.end()].strip()
            titre = s_avant.split('.')[-2]
            journal = s_avant.split('.')[-1]
            auteur = s[:s.find(titre)]
            return '%s\n%s\n%s\n%s\n%s\n'%(auteur.strip(),
                                           titre.strip(),
                                           journal.strip(),
                                           y.strip(), s_apres.strip())
        except:
            return s.strip()
    else:
        return s
    
def parse_html_ref_new(em):
    """
    parse html files from the latest website and return book references
    """
    with open(os.path.join(chemin_html, '%s.html'%em['hash']), 'r') as f:
        s = BeautifulSoup(f.read())
        
    a = s.find("article")

    set_all_li = set(a.find_all('li'))
    
    h = find_livres_h3(a)
    if h: # if the references are in the article
        f = h.findNext('h2')
        lst_art = h.findAllNext('li')
        if f:
            a2 = f.findAllPrevious('li')
            lst_art = list(set(lst_art).intersection(set(a2)))

        lst_art = list(set(lst_art).intersection(set_all_li))

        for i in range(len(lst_art)):
            lst_art[i] = parse_ref_article(lst_art[i])
        
        return lst_art
    
    else: # then maybe the belpw section contains references
        a = s.find(attrs={"class":"content-body-more-details"})
        lst_art = a.findAll('li')
        lst_art = [i for i in lst_art if u"écrit par" in i.text]

        for i in range(len(lst_art)):
            lst_art[i] = parse_ref_section(lst_art[i])

        return lst_art
        
    return None

def parse_ref_section(ref):
    """
    parse les references qui se trouvent dans la section en bas de l'article
    """
    dic_ref = {}
    dic_ref['titre'] = ref.find('a').attrs['title']
    dic_ref['auteur'] = ref.find('span').text.replace(u' écrit par ', '')
    dic_ref['editeur'] = ref.contents[-1][2:-1]

    
    for c, it in dic_ref.items():
        dic_ref[c] = supprimer_ponct_apres(it)
        
    return dic_ref

def parse_ref_article(ref):
    """
    parse les references qui se trouvent dans l'article
    """
    dic_ref = {}
    t = ref.text
    if ref.find('em'):
        dic_ref['titre'] = ref.find('em').text
    if ref.find('strong'):
        dic_ref['titre'] = string_to_unicode(ref.find('strong').text)
    if dic_ref.has_key('titre'):
        pos_titre = t.find(dic_ref['titre'])
        dic_ref['auteur'] = t[:pos_titre].strip()

    if t.strip()[-5:-1].isdigit():
        dic_ref['parution'] = t.strip()[-5:-1]
        
    if dic_ref.has_key('titre') and dic_ref.has_key('parution'):
        pos_fin_titre = t.find(dic_ref['titre']) + len(dic_ref['titre'])
        pos_parution = t.find(dic_ref['parution'])
        dic_ref['editeur'] = t[pos_fin_titre:pos_parution]

    for c, it in dic_ref.items():
        dic_ref[c] = supprimer_ponct_apres(it.strip())
        
    return dic_ref

    
def parse_html_articles_bs(html):
    """
    parse html files from the latest website and return all articles references
    """
    s = BeautifulSoup(html)
    
    s = s.find('article')
    h = find_articles_h3(s)
    if h:
        f = h.findNext('h3')
        lst_art = h.findAllNext('p')
        if f:
            a2 = f.findAllPrevious('p')
            lst_art = list(set(lst_art).intersection(set(a2)))
        return lst_art
    return None
            
def find_articles_h3(s):
    for i in s.findAll('h3'):
        if 'articles' in i.text.lower() and 'scientifiques' in i.text.lower() :
            return i
    return None

def find_livres_h3(s):
    h = None
    for i in s.findAll('h2'):
        if 'livres' in i.text.lower():
            h = i
    if not h:
        for i in s.findAll('h3'):
            if 'livres' in i.text.lower():
                h = i
    return h

def parse_articles_to_manu(id_em, lst_p):
    txt = '/////////////////////// %s\n\n'%id_em

    for p in lst_p:
        a = p.text.split('.')
        if len(a) > 2:
            for i in a:
                txt += i.strip()
                txt += '\n'

    return txt

def actualise_html_hash(sledd_json):
    """
    download the html page of new episodes
    """
    for i in sledd_json['emissions']:
        nom_html = os.path.join(chemin_html, '%s.html'%i['hash'])
        if i.has_key('infos'):
            if i['infos'].has_key('lien_emission') and not os.path.exists(nom_html):
                urllib.urlretrieve(i['infos']['lien_emission'], nom_html)


def get_comments(n_url):
    """
    get the "comments" div from an html page
    """
    soup = BeautifulSoup(urllib.urlopen(n_url).read())
    return soup.find_all('div', 'comment')

if __name__ == '__main__':
    dic_book = {}
    sledd_json = import_json()
    #em_is_missing(sledd_json, download=True)
