#!/env/python
#
# formater les donnees sledd pour jit


def adjacacencie(nodeFrom, nodeTo, couleur):
    txt = 4* '  '+'{\n'                    
    txt += 5*'  ' + '\"nodeTo\":\"%s\",\n' % nodeTo
    txt += 5*'  ' + '\"nodeFrom\": \"%s\",\n' % nodeFrom
    txt += 5*'  ' + '\"data\": {\n' 
    txt += 6*'  ' + '\"$color\": \"%s\",\n' % couleur
    txt += 5*'  ' + '}\n'
    txt += 4*'  ' + '}\n'
    return txt

def adjacencies(i, couleur):
    txt = ''                                                
    txt += 3*'  ' + '\"adjacencies\": [\n'
    for j in i['ids_emission']:
        txt += adjacacencie(i['node'], emissions[j]['node'], couleur)
        txt+= 4*'  ' + ',\n'
    txt = txt[:-10]
    txt += 3*'  ' + '],\n'                         
    return txt

def jit_auteur(i, couleur_ligne, couleur_noeud):
    txt = 2*'  ' + '{\n' + adjacencies(i, couleur_ligne)    
    txt += 3*'  ' + '\"data\": {\n'
    txt += 4*'  ' + '\"$color\": \"%s\",\n' % couleur_noeud
    txt += 4*'  ' + '\"$type\": \"circle\",\n' 
    txt += 4*'  ' + '\"$dim\": %s\n' % len(i['ids_emission'])
    txt += 3*'  ' + '},\n'
    txt += 3*'  ' + '\"id\": \"%s\",\n' % i['node']
    txt += 3*'  ' + '\"name\": \"%s\"\n' % i['nom']
    txt += 2*'  ' + '}\n'
    return txt

def jit_emission(i, couleur_ligne, couleur_noeud):
    txt = 2*'  ' + '{\n' + 3*'  ' + '\"adjacencies\": [],\n'
    txt += 3*'  ' + '\"data\": {\n'
    txt += 4*'  ' + '\"$color\": \"%s\",\n' % couleur_noeud
    txt += 4*'  ' + '\"$type\": \"square\",\n' 
    txt += 4*'  ' + '\"$dim\": %s\n' % 10
    txt += 3*'  ' + '},\n'
    txt += 3*'  ' + '\"id\": \"%s\",\n' % i['node']
    txt += 3*'  ' + '\"name\": \"n°%s\"\n' % i['id_emission']
    txt += 2*'  ' + '}\n'
    return txt
