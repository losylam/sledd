#!/bin/python
# coding: utf-8

import os
import marshal
import subprocess

from sledd_utils import make_trace, choisir, new_aut
from sledd_bdd import actualise_html_hash, parse_html_hash, parse_html_ref_new, import_json
from sledd_articles import get_articles, to_dic_articles


class Sledd:
    def __init__(self, chemin):
        self.emissions = []
        self.chemin_emissions = ''
        self.n_emissions = 0

        self.emissions_importees = False
        self.parties_importees = False

        self.refs = []
        self.auteurs = []
        self.lst_auteurs = []
        self.dic_auteurs = {}

        self.lst_hash = []

        self.lst_ref_wiki = []

        self.articles = []
        self.dic_articles = {}

        self.step = 0
        
    def import_from_file(self, folder):
        """
        get data from the files saved in the folder -folder-
        """
        ff = open(os.path.join(folder, 'refs'), 'rb')
        self.refs = marshal.load(ff)
        ff.close()
        
        ff = open(os.path.join(folder, 'lst_ref_wiki'), 'rb')
        self.lst_ref_wiki = marshal.load(ff)
        ff.close()

        ff = open(os.path.join(folder, 'emissions'), 'rb')
        self.emissions = marshal.load(ff)
        ff.close()

        ff = open(os.path.join(folder, 'auteurs'), 'rb')
        self.auteurs = marshal.load(ff)
        self.lst_auteurs = [i['nom'] for i in self.auteurs]
        self.dic_auteurs = {}
        for i in self.auteurs:
            self.dic_auteurs[i['nom']] = i
        ff.close()
        
        cpt = 0
        for i in self.emissions:
            i['id'] = cpt
            cpt += 1
        for i in self.auteurs:
            i['id'] = cpt
            cpt += 1
        for i in self.lst_ref_wiki:
            i['id'] = cpt
            cpt += 1
                
        for i in self.emissions:
            self.lst_hash.append(i['hash'])
            if i.has_key('autres_hash'):
                for j in i['autres_hash']:
                    self.lst_hash.append(j)

    def save_bdd(self, folder):
        """
        save data in the folder -folder-
        """
        ff = open(os.path.join(folder, 'refs'), 'wb')
        marshal.dump(self.refs, ff)
        ff.close()
        
        ff = open(os.path.join(folder, 'emissions'), 'wb')
        marshal.dump(self.emissions, ff)
        ff.close()

        ff = open(os.path.join(folder, 'auteurs'), 'wb')
        marshal.dump(self.auteurs, ff)
        ff.close()
    
    def import_articles(self, chemin_articles_manu):
        """
        get the articles from file 
        """
        cpt = self.lst_ref_wiki[-1]['id'] +1
        lst_dic = get_articles(chemin_articles_manu)
        dic_articles = to_dic_articles(lst_dic)
        self.articles = dic_articles.values()
        for i in self.articles:
            i['id'] = cpt
            cpt += 1
        self.dic_articles = dic_articles

    def import_json(self):
        self.json = import_json()
        self.dic_hash = {}
        for em in self.json['emissions']:
            self.dic_hash[em['hash']] = em
        
    def actualise_html(self):
        """
        get the html page of
        """
        actualise_html_hash(self.json)

    def check_new_episode(self):
        """
        check if the json database hold new episode and ask if they are rediff
        create a liste -lst_new_em- containing new episodes and a second -lst_old_em-
        containing rediff
        """
        lst_new_em = []
        lst_old_em = []
        for i in self.json['emissions']:
            if i['hash'] not in self.lst_hash:
                print '\n\n///////////////////////// %s'%i['hash']
                print 'redif %s'%i['infos']['rediffusion']
                old_em = self.check_title_exists(i['infos']['titre'], i['hash'])
                if old_em and raw_input('confirm ? [y/n]') in 'oOyY':
                    old_em['autres_hash'].append(i['hash'])
                else:
                    print i['hash'], i['infos']['titre']
                    print 
                    r = raw_input('est une rediff ? [y/n] ')
                    if r not in 'yYoO':
                        dic_new_em = {'hash':i['hash'], 'titre':i['infos']['titre'], 'autres_hash':[]}
                        if i['infos'].has_key('lien_emission'):
                            dic_new_em['lien_emission'] = i['infos']['lien_emission']
                        lst_new_em.append(i)
                    else:
                        lst_old_em.append(i)
        self.lst_new_em = lst_new_em   
        self.lst_old_em = lst_old_em   

    def check_title_exists(self, titre, em_hash):
        tr = make_trace(titre)
        same_title = [i for i in self.emissions if make_trace(i['titre']) == tr]
        if (len(same_title) == 1):
            print 'hash %s'%em_hash
            print 'meme titre em %s : %s'%(same_title[0]['hash'], titre)
            return same_title[0]
        else:
            return None

    def setHashOldEm(self):
        for i in self.lst_old_em:
            print i['infos']['titre']
            h = raw_input('hash original ? (yyyy-mm-dd) :')
            while len(h)!=10:
                print 'pas le bon format de hash'
                h = raw_input('hash de l episode original ? ')
            orig = [em for em in self.emissions if em['hash'] == h]
            if len(orig) == 1:
                print orig[0]['titre']
                confirm = raw_input('confirm ? (o/n) ')
                if confirm in 'OoYy':
                    orig[0]['autres_hash'].append(i['hash'])
                    self.lst_hash.append(i['hash'])
            else:
                print 'ce hash n est pas present dans la liste'

    def getNewRefs(self):
        for em in self.lst_new_em:
            em['ref'] = parse_html_ref_new(em)

    def getNewRef(self, em):
        em['ref'] = parse_html_ref_new(em)
        
    def ajouterEmissions(self, lst_new_em):
        for i in lst_new_em:
            self.ajouterEmission(i)

    def ajouterEmission(self, em):
        dd = {}
        dd['titre'] = em['infos']['titre']
        dd['hash'] = em['hash']
        dd['numero'] = len(self.emissions) + 1
        dd['ids_ref'], dd['new_ref'] = self.trouver_ref(em)
        dd['ids_auteur'] = []
        # ajout des identifiants auteurs a l emission
        for r in dd['ids_ref']:
            for a in self.refs[r]['ids_auteur']:
                dd['ids_auteur'].append(a)
        dd['ids_auteur'] = list(set(dd['ids_auteur']))
        # ajout des identifiants emissions aux auteurs
        for a in dd['ids_auteur']:
            self.auteurs[a]['ids_emission'].append(len(self.emissions))
        # ajout des identifiants emissions aux references
        for a in dd['ids_ref']:
            self.refs[a]['ids_emission'].append(len(self.emissions))
        self.emissions.append(dd)

    def trouver_ref(self, em):
        """
        retrouve la liste des references dans em['ref'] importee depuis parser_html...
        renvoie egalement la liste des references non trouvees
        """
        lst_ref = []
        non_trouve = []
        for r in em['ref']:
            tr = make_trace(r['titre'])
            identiques = [i for i in self.refs if tr == make_trace(i['titre'])]
            if len(identiques) == 1:
                print 'old\t%s\t%s'%(identiques[0]['titre'], identiques[0]['auteur'])
                print 'new\t%s\t%s'%(r['titre'], r['auteur'])
                if raw_input('ok ?') in 'oOyY':
                    lst_ref.append(identiques[0]['id_ref'])
                else:
                    non_trouve.append(r)
            elif len(identiques)>1:
                print '////////////////////////////////////////'
                print 'new\t%s\t%s'%(r['titre'], r['auteur'])
                for no_i, ii in enumerate(identiques):
                    print 'id%s\t%s\t%s'%(no_i+1, ii['titre'],ii['auteur'])
                n_new = int(raw_input('choix ?'))
                if n_new:
                    lst_ref.append(identiques[n_new-1]['id_ref'])
                else:
                    non_trouve.append(r)
            else:
                non_trouve.append(r)
        return lst_ref, non_trouve

    def identifierRefAuteur(self, ref):
        lst_refs = []
        if ref['auteur'] in self.dic_auteurs.keys():            
            lst_refs = [self.refs[i] for i in self.dic_auteurs[ref['auteur']]['ids_ref']]
        if len(lst_refs) >0:
            print 'new\t %s %s'%(ref['titre'], ref['auteur'])
            return choisir(lst_refs, 'titre')
        else:
            return None

    def identifierNewRefsAuteurs(self, em):
        """
        
        """
        lst_suppr = []
        for n_ref in em['new_ref']:
            t = self.identifierRefAuteur(n_ref)
            if t:
                em['ids_ref'].append(t['id_ref'])
                lst_suppr.append(n_ref)
                
        for ref in lst_suppr:
            em['new_ref'].remove(ref)
    
    def identifierNewRefsAuteursTous(self):
        for em in [i for i in self.emissions if len(i['new_ref'])>0]:
            self.identifierNewRefsAuteurs(em)

    def identifierAuteurs(self, ref):
        """
        Renvoi la liste les dictinnaires des auteurs de la
        reference 'ref'
        """
        ref['auteur'] = ref['auteur'].replace(', ', ' et ')
        auts = ref['auteur'].split(' et ')
        output = []
        for aut in auts:
            tr_aut = make_trace(aut)
            lst_id = []
            for aut_old in self.auteurs:
                tr_aut_old = make_trace(aut_old['nom'])
                if tr_aut == tr_aut_old:
                    lst_id.append(aut_old)
            if len(lst_id)>0:
                output.append(choisir(lst_id, 'nom'))
        return output

    def identifierAuteursNewRef(self, em):
        """
        pour l'emission em
        pour les references dans la liste em['new_ref']
        ajoute les numeros des auteurs s'ils sont deja references
        cree un nouvel auteur sinon
        """
        for ref in em['new_ref']:
            tt = self.identifierAuteurs(ref)
            if len(tt)>0:
                ref['ids_auteur'] = [t['id_auteur'] for t in tt]
                ref['id_auteur'] = tt[0]['id_auteur']
            else:
                for aut in ref['auteur'].split(' et '):
                    dd = new_aut(aut)
                    dd['id_auteur'] = len(self.auteurs)
                    self.auteurs.append(dd)

    def identifierAuteursNewRefs(self):
        """
        pour toutes les emissions dont la liste 'new_ref' n'est pas vide
        ajoute les numeros des auteurs s'ils sont deja references
        cree un nouvel auteur sinon
        """
        for i in self.emissions:
            if len(i['new_ref'])>0:
                self.identifierAuteursNewRef(i)
        for i in self.auteurs:
            self.dic_auteurs[i['nom']] = i
        
    def identifierNewRefs(self):
        for i in self.emissions:
            if i.has_key('new_ref'):
                self.identifierNewRefsAuteurs(i)

    def trouverRef(self, ref):
        match = [i for i, it in enumerate(self.refs) 
                 if make_trace(it['titre']) == make_trace(ref['titre']) and make_trace(it['auteur']) == make_trace(ref['auteur'])]
        if len(match) == 1:
            return match[0]
        elif len(match) == 0:
            return None
        else:
            print 'plusieurs references ?'
            return -1

    def ajouterNewRefs(self, em):
        """
        
        """
        for ref in em['new_ref']:
            old = self.trouverRef(ref)
            if old:
                self.ajouterRefExistante(old, em['numero']-1)
            else:
                print ref['auteur'], ref['titre']
                if raw_input('nouvelle reference'):
                    self.ajouterNewRef(ref, em['numero']-1)

    def ajouterNewRefsTout(self):
        for em in self.emissions:
            self.ajouterNewRefs(em)
     
    def ajouterRefExistante(self, i_ref, id_emission):
        if id_emission not in self.refs[i_ref]['ids_emission']:
            self.refs[i_ref]['ids_emission'].append(id_emission)
        if i_ref not in self.emissions[id_emission]['ids_ref']:
            self.emissions[id_emission]['ids_ref'].append(i_ref)
        for i_aut in self.refs[i_ref]['ids_auteur']:
            if id_emission not in self.auteurs[i_aut]['ids_emission']:
                self.auteurs[i_aut]['ids_emission'].append(id_emission)
            if i_aut not in self.emissions[id_emission]['ids_auteur']:
                self.emissions[id_emission]['ids_auteur'].append(i_aut)

    def ajouterNewRef(self, ref, id_emission):
        # ajout liens vers emissions et auteurs
        ref['id_emission'] = id_emission
        ref['ids_emission'] = [id_emission]
        ref['id_ref_wiki'] = None
        ref['ids_ref_wiki'] = []
        ref['plus'] = []
        auts = ref['auteur'].split(' et ')
        ref['id_auteur'] = self.dic_auteurs[auts[0]]['id_auteur']
        ref['ids_auteur'] = []
        for aut in auts:
            ref['ids_auteur'].append(self.dic_auteurs[aut]['id_auteur'])
        ref['id_ref'] = len(self.refs)

        # ajout ref 
        self.refs.append(ref)
        self.ajouterRefExistante(ref['id_ref'], ref['id_emission'])

    def actualiserEpisodes(self):
        self.import_json()
        self.check_new_episode()
        self.actualise_html()
        self.getNewRefs()
        self.ajouterEmissions(self.lst_new_em)
        self.identifierNewRefsAuteursTous()
        self.identifierAuteursNewRefs()
        self.ajouterNewRefsTout()
        self.actualiserLiens()
        self.actualiserLiens()

    def actualiserEpidose(self, em, reprendre = False):
        if not reprendre:
            self.step = 0
            
        if self.step == 0:
            self.ajouterEmission(em)
            raw_input('Étape suivante ?')
            self.step += 1
            
        if self.step == 1:
            self.identifierNewRefsAuteursTous()
            raw_input('Étape suivante ?')
            self.step += 1

        if self.step == 2:
            self.identifierAuteursNewRefs()
            raw_input('Étape suivante ?')
            self.step += 1

        if self.step == 3:
            self.ajouterNewRefsTout()
            raw_input('Finalisé ?')
            self.step += 1
            
        if self.step == 4:
            self.ajouterNewRefsTout()
            self.actualiserLiens()
            self.actualiserLiens()

            self.emissions[-1]['new_ref'] = []
            self.step = 0
            
    def actualiserLienEmissions(self):
        for i in self.emissions:
            if not i.has_key('lien_emission'):
                i['lien_emission'] = self.dic_hash[i['hash']]['infos']['lien_emission']
            if not i.has_key('autres_hash'):
                i['autres_hash'] = []

    def actualiserLiens(self):
        print '////////////////////////////'
        print 'from emissions'
        for i in self.emissions:
            i_em = i['numero'] - 1
            for j in i['ids_auteur']:
                if i_em not in self.auteurs[j]['ids_emission']:
                    print '%s(%s) not in %s(%s)'%(i['titre'], 
                                                  i_em,
                                                  self.auteurs[j]['nom'],
                                                  j)
                    self.auteurs[j]['ids_emission'].append(i_em)
            for j in i['ids_ref']:
                if i_em not in self.refs[j]['ids_emission']:
                    print '%s(%s) not in %s(%s)'%(i['titre'], 
                                                  i_em,
                                                  self.refs[j]['titre'],
                                                  j)
                    self.refs[j]['ids_emission'].append(i_em)

        print
        print '////////////////////////////'
        print 'from auteurs'
        for i in self.auteurs:
            i_aut = i['id_auteur']
            for j in i['ids_emission']:
                if i_aut not in self.emissions[j]['ids_auteur']:
                    print '%s(%s) not in %s(%s)'%(i['nom'], 
                                                  i_aut,
                                                  self.emissions[j]['titre'],
                                                  j)
                    self.emissions[j]['ids_auteur'].append(i_aut)
            for j in i['ids_ref']:
                if i_aut not in self.refs[j]['ids_auteur']:
                    print '%s(%s) not in %s(%s)'%(i['nom'], 
                                                  i_aut,
                                                  self.refs[j]['titre'],
                                                  j)
                    self.refs[j]['ids_auteur'].append(i_aut)
            
        print
        print '////////////////////////////'
        print 'from refs'
        for i in self.refs:
            i_ref = i['id_ref']
            for j in i['ids_emission']:
                if i_ref not in self.emissions[j]['ids_ref']:
                    print '%s(%s) not in %s(%s)'%(i['titre'], 
                                                  i_ref,
                                                  self.emissions[j]['titre'],
                                                  j)
                    self.emissions[j]['ids_ref'].append(i_ref)
            for j in i['ids_auteur']:
                if i_ref not in self.auteurs[j]['ids_ref']:
                    print '%s(%s) not in %s(%s)'%(i['titre'], 
                                                  i_ref,
                                                  self.auteurs[j]['nom'],
                                                  j)
                    self.auteurs[j]['ids_ref'].append(i_ref)
                for k in i['ids_emission']:
                    if k not in self.auteurs[j]['ids_emission']:
                        print '%s(%s) not in %s(%s)'%(self.emissions[k]['titre'], 
                                                      k,
                                                      self.auteurs[j]['nom'],
                                                      j)
                        self.auteurs[j]['ids_emission'].append(k)

    def printByName(self, txt, out = False):
        lst_em = [it for it in self.emissions if txt in it['titre']]
        lst_aut = [it for it in self.auteurs if txt in it['nom']]
        lst_ref = [it for it in self.refs if txt in it['titre']]
        
        if len(lst_em)>0:
            print("Emissions :")
            for it in lst_em:
                print('em %s %s /// %s' %(it['hash'], it['id'], it['titre']))
            print
        if len(lst_aut)>0:
            print("Auteurs :")
            for it in lst_aut:
                print('aut %s /// %s' %(it['id_auteur'], it['nom']))
            print
        if len(lst_ref)>0:
            print("References :")
            for it in lst_ref:
                print('ref %s /// %s' %(it['id_ref'], it['titre'])) 
            print
        if out:
            return lst_em + lst_aut + lst_ref

    def remplacerRef(self, ref_bad, ref_good, replace_author = True):
        id_bad = ref_bad['id_ref']
        id_good = ref_good['id_ref']
        for id_em in ref_bad['ids_emission']:
            em = self.emissions[id_em]
            # remplace ref dans la liste de references de l emission
            em['ids_ref'][em['ids_ref'].index(id_bad)] = id_good
            if replace_author:
                # retire les auteurs de la mauvaise reference et ajoute les bons dans 
                # la liste des auteurs lies a l emission
                set_moins = set(em['ids_auteur']).difference(set(ref_bad['ids_auteur']))
                set_plus = set_moins.union(set(ref_good['ids_auteur']))
                em['ids_auteur'] = list(set_plus)

                # supprime l emissions dans l auteur de la mauvaise ref
                # et l ajoute dans l'autre
                for id_aut in ref_bad['ids_auteur']:
                    self.auteurs[id_aut]['ids_emission'].remove(em['id'])
                for id_aut in ref_good['ids_auteur']:
                    self.auteurs[id_aut]['ids_emission'].append(em['id'])

        ref_good['ids_emission'] += ref_bad['ids_emission']
        ref_bad['ids_emission'] = []

    def remplacerAuteur(self, aut_bad, aut_good):
        id_bad = aut_bad['id_auteur']
        id_good = aut_good['id_auteur']
        for id_em in aut_bad['ids_emission']:
            em = self.emissions[id_em]
            # remplace ref dans la liste de references de l emission
            em['ids_auteur'][em['ids_auteur'].index(id_bad)] = id_good
       
        for id_ref in aut_bad['ids_ref']:
            ref = self.refs[id_ref]
            # remplace ref dans la liste de references de l emission
            ref['ids_auteur'][ref['ids_auteur'].index(id_bad)] = id_good

        aut_good['ids_ref'] += aut_bad['ids_ref']
        aut_bad['ids_ref'] = []
        aut_good['ids_emission'] += aut_bad['ids_emission']
        aut_bad['ids_emission'] = []

    def fusionner_emission(self, i_em1, i_em2):
        # enlever la reference a l'emission i_em2 dans les references et auteurs
        for ref in self.refs:
            if i_em2 in ref['ids_emission']:
                ref['ids_emission'].remove(i_em2)
            
        for aut in self.auteurs:
            if i_em2 in aut['ids_emission']:
                aut['ids_emission'].remove(i_em2)

        # soustraire 1 a toutes les references aux emissins apres i_em2
        for ref in self.refs:
            for i, id_em in enumerate(ref['ids_emission']):
                if id_em > i_em2:
                    ref['ids_emission'][i] = ref['ids_emission'][i]-1 
            if ref['id_emission'] > i_em2:
                ref['id_emission'] = ref['id_emission'] - 1

        for aut in self.auteurs:
            for i, id_em in enumerate(aut['ids_emission']):
                if id_em > i_em2:
                    aut['ids_emission'][i] = aut['ids_emission'][i]-1

        # enlever l'emission i_em2
        self.emissions.pop(i_em2)

        for i_em, em in enumerate(self.emissions):
            em['id'] = i_em
            em['numero'] = i_em + 1
