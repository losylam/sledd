#!/bin/python
#coding: utf-8

import eyed3

chemin_emissions = "/media/laurent/40b145bf-73e0-4067-84c2-ca8aeed6258c/Sledd/emissions_tags"

def find_in_json(hash_name, json):
    l = [i for i in json['emissions'] if i['hash'] == hash_name]
    if len(l)==1:
        return l[0]
    else:
        return None

def write_tags(filename, json_file):
    hash_name = filename[:-4]
    emission = find_in_json(hash_name, json_file)
    if emission:

        print(u"écriture tag: %s"%hash_name)
        audiofile = eyed3.load(os.path.join(chemin_emissions, filename))
        audiofile.initTag()
        audiofile.tag.artist = u"Jean-Claude Ameisen"
        audiofile.tag.album = u"Sur les épaules de Darwin"
        audiofile.tag.album_artist = u"Jean-Claude Ameisen"
        audiofile.tag.title = hash_name + " " + unicode(emission["infos"]["titre"])
        audiofile.tag.release_date = emission["hash"]
        audiofile.tag.date = emission["hash"]
        audiofile.tag.genre = u"Podcast"
        audiofile.tag.track_num = json_file["emissions"].index(emission)

        audiofile.tag.save(encoding="utf-8")

    else:
        print(u"émission non trouvée: %s"%hash_name)
