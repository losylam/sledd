#!/bin/python

from sledd import *
from sledd_utils import *
from sledd_articles import *

chemin_emissions_man = '/home/laurent/Projets/Sledd/bdd/emissions_man'
chemin_donnees = '/home/laurent/Projets/Sledd/bdd'
chemin_articles = '/home/laurent/Projets/Sledd/bdd/articles_manu'

sledd = Sledd(chemin_emissions_man)
sledd.import_from_file(chemin_donnees)
sledd.import_articles(chemin_articles)

# sledd.lst_new_em = marshal.load(open('../tmp/lst_new_em'))
# sledd.getNewRefs()

# def next():
#     sledd.save_bdd(chemin_donnees)
#     sledd.lst_new_em = sledd.lst_new_em[1:]
#     marshal.dump(sledd.lst_new_em, open('../tmp/lst_new_em', 'w'))
