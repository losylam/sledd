#!/env/python

from sledd_utils import *


def get_articles(nom_article_manu):
    t = open(nom_article_manu, 'r').read().split('\n\n')

    lst_em = []
    new_list = []
    lst_dic = []

    for i in t:
        if i[:5] == '/////':
            lst_em.append(new_list)
            new_list = []
        else:
            new_list.append(i)
            
    for i_em, d in enumerate(lst_em):
        for i, it in enumerate(d):
            if it[0] != '/':
                tt = it.split('\n')
                di = {}
                di['auteurs'] = tt[0].strip()
                di['titre'] = tt[1].strip()
                di['em'] = i_em
                if len(tt) >= 2:
                    di['revue'] = supprimer_ponct_apres(tt[2].strip())
                if len(tt) >= 3:
                    di['annee'] = supprimer_ponct_apres(tt[3].strip())
                if len(tt) >= 4:
                    di['infos'] = tt[4:]
                lst_dic.append(di)

    return lst_dic


def to_dic_articles(lst_dic):
    dic_articles = {}

    for i in lst_dic:
        tr = make_trace(i['titre'])
        if not dic_articles.has_key(tr):
            dic_articles[tr] = i
            dic_articles[tr]['lst_em'] = [i['em']]
            dic_articles[tr]['lst_aut'] = [i['auteurs']]
            dic_articles[tr]['lst_titres'] = [i['titre']]
            if i.has_key('revue'):
                dic_articles[tr]['lst_rev'] = [i['revue']]
            else:
                dic_articles[tr]['lst_rev'] = []
            if i.has_key('annee'):
                dic_articles[tr]['lst_annee'] = [i['annee']]
            else:
                dic_articles[tr]['lst_annee'] = []
        else:
            dic_articles[tr]['lst_em'].append(i['em'])
            if i['auteurs'] not in dic_articles[tr]['lst_aut']:
                dic_articles[tr]['lst_aut'].append(i['auteurs'])
            if i['titre'] not in dic_articles[tr]['lst_titres']:
                dic_articles[tr]['lst_titres'].append(i['titre'])
            if i['annee'] not in dic_articles[tr]['lst_annee'][0]:
                dic_articles[tr]['lst_annee'].append(i['annee'])
            if i['revue'] not in dic_articles[tr]['lst_rev']:
                dic_articles[tr]['lst_rev'].append(i['revue'])
           
    return dic_articles


def get_auteurs(dic_articles):
    dic_auteurs = {}
    for c, i in dic_articles.items():
        i['auts'] = i['auteurs'].split(', ')
        if 'et coll' in i['auts'][-1]:
            i['auts'] = i['auts'][:-1]
            i['et_coll'] = True
        else:
            i['et_coll'] = False
        for aut in i['auts']:
            aut = supprimer_ponct_apres(aut.strip())
            if not dic_auteurs.has_key(aut):
                dic_auteurs[aut] = {'nom':aut, 'lst_art':[i]}
            else:
                dic_auteurs[aut]['lst_art'].append(i)
    return dic_auteurs


def get_dic_revues(dic_art):
    dic_revues = {}
    for c, i in dic_art.items():
        tr = make_trace(i['revue'])
        if dic_revues.has_key(tr):
            dic_revues[tr]['lst_articles'].append(i)
            if i['revue'] not in dic_revues[tr]['lst_titres']:
                dic_revues[tr]['lst_titres'].append(i['revue'])
        else:
            dic_revues[tr] = {'lst_titres':[], 'lst_articles':[]}
            dic_revues[tr]['lst_titres'].append(i['revue'])
            dic_revues[tr]['lst_articles'].append(i)
    return dic_revues


if __name__ == '__main__':
    nom_articles_manu = 'Projets/Sledd/bdd/articles_manu'
    lst_dic = get_articles(nom_articles_manu)
    dic_articles = to_dic_articles(lst_dic)
    dic_auteurs = get_auteurs(dic_articles)
    dic_revues = get_dic_revues(dic_articles)
