from sledd import *
from sledd_bdd import *
from sledd_utils import *
from sledd_articles import *


chemin_emissions_man = '/home/laurent/Projets/Sledd/bdd/emissions_man'
chemin_donnees = '/home/laurent/Projets/Sledd/bdd'
chemin_articles = '/home/laurent/Projets/Sledd/bdd/articles_manu'

sledd = Sledd(chemin_emissions_man)
sledd.import_from_file(chemin_donnees)
sledd.import_articles(chemin_articles)


txt = ""
for i in sledd.emissions[186:]:
    with open('../html/'+i['hash']+'.html', 'r') as f:
        lst_p = parse_html_articles_bs(f.read())
        if lst_p:
            txt += parse_articles_to_manu(i['id'], lst_p)
