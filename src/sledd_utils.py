#!/bin/python
# -*- coding: utf-8 -*-


def export_bib(refs):
    """
    exporte les references au format bibtex
    """
    bib = ''
    lst_cles_bib = []
    abc = 'abcdefghijklmnopqrstuvwxyz'
    for i in refs:
        bibkey = '%s_%s'%(i['auteur'].split()[-1], i['parution'])
        if bibkey in lst_cles_bib:
            cpt = 0
            bibkey += abc[cpt]
            while bibkey in lst_cles_bib:
                cpt += 1
                bibkey = bibkey[:-1]+abc[cpt]
        bib += '@book{%s,\n' % bibkey
        bib += '\ttitle = \"%s\",\n' % i['titre']
        bib += '\tauthor = \"%s\",\n' % i['auteur']
        if i.has_key('\xc3\xa9diteur'):
            bib += '\tpublisher = \"%s\",\n' % i['\xc3\xa9diteur']
        if i.has_key('parution'):
            bib += '\tyear = \"%s\",\n' % i['parution']
        bib += '}\n\n'
        lst_cles_bib.append(bibkey)
    return bib


def supprimer_ponct_apres(chaine):
    if len(chaine)>1 and chaine[-1] in ',.;:':
        return chaine[:-1]
    else:
        return chaine


def nettoyer_double_n(texte):    
    lst_texte = texte.split('\n')
    for i in range(len(lst_texte)):
        if lst_texte[i].isspace():
            lst_texte[i] = ''
    texte_out = ''
    for i in lst_texte:
        texte_out+=i+'\n'
    texte_out = texte_out[:-1]
    return texte_out
            

def sel_apres(chaine, repere):
    tt = chaine
    if repere in chaine:
        tt = tt[tt.find(repere)+len(repere):]
        tt = tt[tt.find('\n')+1:]
        return tt
    else:
        return ''


def make_trace(mot):
    lettres = 'abcdefghijklmnopqrstuvwxyz1234567890'
    sortie = ''
    for lettre in mot.lower():
        if lettre in lettres:
            sortie+=lettre
    return sortie


def choisir(liste_dic, champ):
    for i, it in enumerate(liste_dic):
        print '[%s]\t%s'%(i+1, it[champ])
    c = int(raw_input('choix ? (0 si aucun) '))
    if (c == 0):
        return None
    else:
        return liste_dic[c-1]


def new_aut(nom):
    print '/// nouvel auteur'
    print nom
    dd = {}
    dd['nom'] = nom
    dd['wikipedia_infobox'] = None
    dd['ids_emission'] = []
    dd['ids_ref'] = []
    dd['pb'] = False
    dd['wikipedia_texte'] = None
    dd['wikipedia_titre'] = raw_input('wiki_titre ')
    dd['profession'] = raw_input('profession ')
    dd['naissance'] = raw_input('naissance ').split()
    dd['deces'] = raw_input('deces ').split()
    dd['wikipedia_langue'] = raw_input('wiki_lang ')
    print
    return dd


def unicode_to_string(ch):
    """
    Converti une chaine unicode en string utf-8
    """
    if type(ch)==unicode:
        return str(ch.encode('utf-8'))
    else:
        return ch

def string_to_unicode(ch):
    """
    Converti une string en unicode
    """
    if type(ch)==str:
        return unicode(ch.encode('utf-8'))
    else:
        return ch

    

def parse_articles(ff):
    """
    converti un fichier avec des articles
    """
    f = open(ff)
    l = f.readlines()
    l = [i for i in l if not i.isspace()]
    lst_dic = []
    for i in l:
        if i[0] == '/':
            lst_dic.append({'ligne0':i, 'articles':[]})
        else:
            lst_dic[-1]['articles'].append(str2art(i))
    return lst_dic


def format_lst_article(ff):
    lst_dic = parse_articles(ff)
    txt = ''
    for i in lst_dic:
        txt += i['ligne0']+'\n'
        for art in i['articles']:
            txt += art['aut']
            txt += art['titre']
            txt += art['journal']
            txt += art['year']
            txt += art['suite']
    return txt
        

def str2art(chaine):
    """
    converti une chaine contenant une reference en un dictionnaire
    """
    dic_art = {'aut':'', 'titre':'', 'journal':'','year':''}
    lst_chaine = chaine.split('. ')
    if len(lst_chaine) == 3:
        dic_art['aut'] = lst_chaine[0]+'.\n'
        dic_art['titre'] = lst_chaine[1]+'.\n'
        journal_year = lst_chaine[2]
        lst_year = [y for y in journal_year.split() if len(y)>4 and y[:4].isdigit()]
        if len(lst_year) > 0:
            journal_year_split = journal_year.split(lst_year[0])
            if len(journal_year_split) == 2:
                dic_art['journal'] = journal_year_split[0].lstrip()+'\n'
                dic_art['suite'] = journal_year_split[1].lstrip()+'\n'
                dic_art['year'] = lst_year[0]+'\n'
            else:
                dic_art['suite'] = journal_year
        else:
            dic_art['suite'] = journal_year
    else:
        dic_art['suite'] = chaine + '\n'
    return dic_art

