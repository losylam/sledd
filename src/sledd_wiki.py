#!/bin/python
# -*- coding: utf-8 -*-

import urllib
import json
import subprocess

def get_info_wikipedia(sledd):
    for i in sledd.auteurs:
        tt = importer_from_wp(i['nom'])
        i['wikipedia_texte'] = tt
        if tt:                                    
            i['wikipedia_infobox'] = get_info_box(tt)
        else:
            i['wikipedia_infobox'] = None


def importer_from_wp(nom):
    pages = importer_from_wp_raw(nom)
    if len(pages.keys())>1:
        print 'plusieurs pages ??'
    if '-1' in pages.keys():
        return None
    else:
        id_page = pages['query']['pages'].keys()[0]
        return pages[id_page]['revisions'][-1]['*']


def importer_from_wp_raw(nom):
    page = urllib.urlopen('http://fr.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=%s'%nom)
    inpage = json.load(page)
    pages = inpage['query']['pages']
    return pages


def get_info_box(wiki_txt):
    if 'Infobox' in wiki_txt:
        txt = wiki_txt[wiki_txt.find('{{Infobox')+2:]
        txt = txt[:txt.find('\n}}\n')]
        return txt
    else:
        return None


def parse_info_box(txt):
    txt = txt.split('\n')
    if 'Écrivain' not in txt[0] and 'Philosophe' not in txt[0] and 'Scientifique' not in txt[0]:
        return txt[0]


def get_date_lines(auteur):
    txt = auteur['wikipedia_infobox'].encode('utf8').split('\n')
    if len(txt)>1:
        for i in txt:
            if 'naissance' in i and ('date' in i or 'Date' in i):
                ll = i[i.find('{{')+2:i.find('}}')]
                ll = ll.split('|')
                if len(ll)>3:
                    jour = ll[1]
                    mois = ll[2]
                    annee = ll[3]
                    inp = raw_input('%s -> %s/%s/%s ?? '%(auteur['nom'], jour, mois, annee))
                    if not inp:
                        auteur['naissance'] = [jour, mois, annee]


def get_date_deces_lines(auteur):
    txt = auteur['wikipedia_infobox'].encode('utf8').split('\n')
    if len(txt)>1:
        for i in txt:
            if 'décès' in i and ('date' in i or 'Date' in i):
                ll = i[i.find('{{')+2:i.find('}}')]
                ll = ll.split('|')
                if len(ll)>3:
                    jour = ll[1]
                    mois = ll[2]
                    annee = ll[3]
                    inp = raw_input('%s -> %s/%s/%s ?? '%(auteur['nom'], jour, mois, annee))
                    if not inp:
                        auteur['décès'] = [jour, mois, annee]


def get_profession(auteur):
    txt = auteur['wikipedia_infobox'].encode('utf8').split('\n')
    if len(txt)>1:
        if 'biographie' not in txt[0] and 'Biographie' not in txt[0]:
            if '_' in txt[0]:
                outt = txt[0].split('_')
                print auteur['nom']
                auteur['profession'] = confirm(outt[1])
            elif ' ' in txt[0]:
                outt = txt[0].split(' ')
                print auteur['nom']
                auteur['profession'] = confirm(outt[1])
        else:
            auteur['profession'] = confirm(auteur['nom'])


def confirm(chaine):
    tt = raw_input('%s ??'%chaine)
    if tt:
        return tt
    else:
        return chaine


def get_profession_autres(auteur):
    if (auteur['wikipedia_texte'] != None) and ('profession' not in auteur.keys()):
        print auteur['nom']
        chemin_web = 'http://fr.wikipedia.org/wiki/%s'%(auteur['nom'].replace(' ', '_'))
        print chemin_web
        subprocess.call(['firefox', chemin_web])
        print auteur['nom']
        tt = raw_input('profession ? ')
        if tt:
            auteur['profession'] = tt.split()
        tt = raw_input('naissance ? ')
        if tt:
            auteur['naissance'] = tt.split()
        tt = raw_input('deces ? ')
        if tt:
            auteur['décès'] = tt.split()
        print '\n\n********************************\n'
