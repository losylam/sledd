==Retranscription==


==Crédits phonographiques==
Lisa Portelli - Le Régal 

Pierre Bondu - Vu d'ici

Pink Martini - Amado Mio

Cults - Bumper

Blossom Dearie - Chez moi (1959)


==Crédits bibliographiques==

=Citations=

 Qu’est-ce que le temps ?
 Comment donc ces deux temps, le passé et l’avenir, sont-ils, puisque le passé n’est plus et que l’avenir n’est pas encore ?
 Quant au présent, s’il était toujours présent, s’il n’allait pas rejoindre le passé, il ne serait pas du temps, il serait de l’éternité
Saint Augustin. Les Confessions. Livre XI.

 Au mot présent, il faut préférer le mot plus sûr de passant.
 Le présent est le passant du temps. […]
 Il est possible que dans le passant du temps, le passé soit l’énergie. Comme le mot courant dit quelque chose de plus profond que 
 toute l’eau du fleuve. […] 
 Nous ne connaissons jamais ce qui commence à son début. […]
 Nous avons vécu avant de naître. Nous avons connu la vie avant que le soleil n’éblouisse nos yeux. […]
 « Han Yu naquit en 768. […] Un jour il déploya les cinq doigts de sa main. Il dit énigmatiquement qu’il avait encore entre chacun de  
 ses doigts l’ombre de la première aube. […] 
 Retrouver l’aube partout, partout, partout, c’est une façon de vivre.
 Reconstituer la naissance dans tout automne ; héler la perdue dans l’introuvable ; faire resurgir l’autre incessant et imprévisible 
 dans l’irruption de la première fois car il n’en est pas d’autres.
 Naître.
Pascal Quignard. Les ombres errantes.

 Et pendant que cette planète continuait à tourner selon la loi fixe de la gravitation, à partir d’un début si simple, une infinité de 
 formes les plus belles et les plus merveilleuses ont évolué, et continuent d’évoluer.
Charles Darwin. De l’origine des espèces.

 Le soleil qui court sur le monde 
 J’en suis certain comme de toi
 Le soleil met la terre au monde. 
Paul Eluard. Aube.

 Je levais les yeux.
 Contempler le ciel, qui n’est pas vivant, pour tout ce qui est vivant, c’est contempler le seul aïeul. »
Pascal Quignard. Les ombres errantes.

=Articles=

-Colwell CS. Linking neural activity and molecular oscillations in the SCN. Nature Reviews/Neuroscience 2011, 12:553-69. 
 
-Ferrell JE Jr, Tsai TY, Yang Q. Modeling the cell cycle: why do certain circuits oscillate? Cell 2011, 144:874-85. 
 
-Rust MJ, Golden SS, O'Shea EK. Light-driven changes in energy metabolism directly entrain the cyanobacterial circadian oscillator. Science 2011, 331:220-3.
 
-Hogenesch JB, Ueda HR. Understanding systems-level properties: timely stories from the study of clocks. Nature Reviews/Genetics. 2011, 12:407-16.  
 
-Bass J, Takahashi JS. Circadian integration of metabolism and energetics. Science 2010, 330:1349-54.
 
-Reddy AB, O’Neill JS. Healthy clocks, healthy body, healthy mind. Trends in Cell Biology 2010, 20:36–44. 
 
-Zhang E, Kay S. Clocks not winding down : unravelling circadian networks. Nature Reviews/Molecular Cell Biology. 2010, 11:764-76. 
 
-Kondo S, Miura T. Reaction-diffusion model as a framework for understanding biological-pattern formation. Science. 2010, 329:1616-20. 
 
-Keller M, Mazuch J, Abraham U, et coll. A circadian clock in macrophages controls inflammatory immune responses. Proceedings of the National Academy of Sciences U S A 2009, 106:21407-12. 
 
-Dodd AN, Salathia N, Hall A, et coll. Plant circadian clocks increase photosynthesis, growth, survival, and competitive advantage. Science 2005, 309:630-3. 
 
-Ouyang Y, Andersson CR, Kondo T, et coll. Resonating circadian clocks enhance fitness in cyanobacteria. Proceedings of the National Academy of Sciences U S A 1998, 95:8660-4. 
 
-Konopka R, Benzer S. Clock mutants of drosophila melanogaster. Proceedings of the National Academy of Sciences USA. 1971, 68:2112-6. 
 
-Turing A. The chemical basis of morphogenesis. Philosophical Transactions of the Royal Society of London. Series B. Biological Sciences. 1952, 237:37-72. 

=Ouvrages=

L’Origine des espèces
de Charles Darwin
parution : 2008

J’ai un visage pour être aimé
de Paul Eluard
éditeur : Editions Gallimard
parution : 2009

La Vie oscillatoire. Au cœur des rythmes du vivant
de Albert Goldbeter
éditeur : O Jacob
parution : 2010

Les Ombres errantes : Tome 1, Dernier royaume
de Pascal Quignard
éditeur : Gallimard collection Folio
parution : 2002

Les Confessions
de Saint Augustin
éditeur : Editions Flammarion
parution : 1993
