
 Désolé pour une erreur d’inattention survenue pendant l'enregistrement de l'émission: la structure cyclique du benzène forme un hexagone (et non pas un losange). 
JC Ameisen.

==Retranscription==



==Crédits phonographiques==
Qluster - live bei more Ohr Less. Lunz August 2012 

Nino Rota - o_venezia,_venaga,_venusia 

Charles Koechlin - Jeux dans la clairière - Dans la forêt antique (les chants de nectaire livre II) 

Wax Taylor - City Vapors 

Extrait Livre disque - "A moveable Feast" lu par Ernest Hemingway 

Myguk - Epilogue 

Roedelius &amp; Schneider - Country 

Rachel Portman - come home with me 

Roedelius &amp; Schneider - Miniatur 

Bernard Parmegiani - des mots et des sons

DJ Omnix - symbols 

Edith Progue - 4PM 

Efterklang - Hands Playing butterfly 

DJ Omnix - symbols 

Wax Taylor - my window 

Jean-Pierre Drouet - apparition 

Theme of Korobeiniki (sample jeux Tetris) 

Myguk - le dîner 

Blake Neely - your worst nightmare

François &amp; The Atlas Mountains - City Kiss

Patrick Watson - step out for a while

Fabio Viscogliosi - Di Stilli

==Crédits bibliographiques==

=Citations=

 J’étais déjà bien engagé, sur le chemin du sommeil, Et je pouvais dire
 Quelle forme mon rêve allait prendre.
 Des pommes géantes apparaissent et disparaissent,
 Extrémité de la tige, et extrémité de la fleur
 Et chacune de leurs petites taches rousses se révèle tendrement.
 Non seulement ma voûte plantaire conserve la douleur,
 Mais je sens aussi l’échelle qui tangue, pendant que les branches des pommiers plient.
Robert Frost. After apple-picking [Après avoir cueilli des pommes]

 Des souvenirs, vagues ou intenses, se reconfigurent pendant que l’artiste joue. Des compagnons imaginaires surgissent de régions inconnues pour nous tenir compagnie.
 De même que la mémoire et les rêves, la fiction transforme un matériau profondément émotionnel en des histoires qui ont un sens.
 C’est alors que fleurissent, comme des rêves, les fictions de la littérature, recouvrant, dans une tentative de maîtrise, nos blessures et nos pertes.
 Écrire de la fiction, c’est comme rêver, alors qu’on est éveillé.
Siri Hustvedt. Living, thinking, looking [Vivre, penser, voir]

 Ce Promontoire du Songe, dont nous venons de parler, il est dans Shakespeare, il est dans tous les grands poètes.
 Dans le monde mystérieux de l’art, comme dans cette Lune où notre regard abordait tout à l’heure, il y a la cime du rêve.
 À cette cime du rêve est appuyée l’échelle de Jacob. Jacob couché au pied de l’échelle, c’est le poète, ce dormeur qui a les yeux de l’âme ouverts.
 Cette cime du Rêve est un des sommets qui dominent l’horizon de l’art.
 Tout songeur a en lui ce monde imaginaire. Cette cime du rêve est sous le crâne de tout poète, comme la montagne sous le ciel. 
 Donc, songez, poètes ; songez, artistes ; songez, philosophes ; penseurs, soyez rêveurs.
 L’Homme a besoin du rêve.
 Platon rêve l’Atlantide, Dante le Paradis, Milton l’Éden, Thomas Morus la Cité Utopia, Campanella la Cité du Soleil, Hall la Mundus Alter, Cervantes Barataria, Fénelon Salente.
 Seulement n’oubliez pas ceci : il faut que le songeur soit plus fort que le songe.
Victor Hugo. Le Promontoire du Songe.

 Apprenons à rêver, Messieurs, et alors, peut-être, nous découvrirons la vérité.
 Apprenons à rêver, mais gardons-nous de révéler nos rêves tant qu’ils n’auront pas été évalués et éprouvés par la raison, par l’entendement de nos états de veille.
August Kekulé. Discours devant la Société Allemande de Chimie, en 1890, à l’occasion de la célébration du vingt-cinquième anniversaire de sa découverte de la structure du benzène.

=Articles=

-Wamsley E, Stickgold R. Dreaming and offline memory processing. Current Biology 2010, 20:R1010-13.
 
-Walker MP, Stickgold R. Overnight alchemy: sleep-dependent memory evolution. Nature Reviews Neuroscience 2010, 11:218.
 
-Diekelmann S, Born J. The memory function of sleep. Nature Reviews Neuroscience 2010, 11:114-26.
 
-Stickgold R, Malia A, Maguire D, et coll. Replaying the game: Hypnagogic images in normals and amnesics. Science 2000, 290:350-3.
 
-Helmuth L. Neuroscience. Video game Images persist despite amnesia. Science 2000, 290:247-9.

=Ouvrages=

Winter Journal
de Paul Auster
éditeur : Henry Holt &amp; Company
parution : 2012

Sur le rêve
de Sigmund Freud
éditeur : Point Seuil
parution : 2011

L'interprétation du rêve
de Sigmund Freud
éditeur : Puf Quadrige
parution : 2010

Poems: A boy's will and North of Boston
de Robert Frost
éditeur : Signet classic
parution : 2010

Paris est une fête
de Ernest Hemingway
éditeur : Folio
parution : 2012

Le Promontoire du Songe
de Victor Hugo
éditeur : Gallimard / L'Imaginaire
parution : 2012

Living, thinking, looking. Essays
de Siri Hustvedt
éditeur : Picador
parution : 2012

Einstein's dreams
de Alan Lightman
éditeur : Corsair
parution : 2012

L'oeil de l'esprit
de Oliver Sacks
éditeur : Editions du Seuil
parution : 2012


==Agenda==

« Les Battements du Temps »  
- Rencontres transdisciplinaires du Centre d’Études du Vivant,
Université Paris Diderot, Institut des Humanités de Paris.
Lieu : Amphi Buffon, 15 rue Hélène Brion - 75013 Paris.  
RER C /M° ligne 14 - Bibliothèque François Mitterrand ; Bus : 62, 64, 89, 132, 325. 
Entrée libre (réservation conseillée par e-mail : centre_etudes_du_vivant@univ-paris-diderot.fr )
Attention: il n'y a plus de place pour la Rencontre du 18 septembre: réservez dès que possible pour celle du 9 octobre). 
Prochaines rencontres :
- Mardi 18 septembre 2012, de 19h à 21h : « Le temps est-il un cas de conscience ? » 
avec Etienne Klein, physicien, et Jean Claude Ameisen, discutant.
- Mardi 9 octobre : « Vivre avec les bêtes », Elisabeth de Fontenay, philosophe.
 
- A Strasbourg, le samedi 22 septembre,
Vous êtes invités à participer à la Réflexion publique autour de l’accompagnement des personnes en fin de vie.
de 10h à 13h, Ateliers de réflexion collective, 
de 14h30 à 17h30, échanges et débat.
La mission, présidée par le Pr Didier Sicard, recueillera vos avis et questions
à la Faculté des Sciences Economiques et de Gestion,
61 Avenue de la Forêt Noire  67000 Strasbourg.
