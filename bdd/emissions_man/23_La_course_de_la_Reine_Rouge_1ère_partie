==Retranscription==

Nous percevons du monde qui nous entoure un foisonnement de reflets, de formes, de mouvements, de couleurs, de sons, d’odeurs et de sensations de toucher. Et à partir de ces perceptions parcellaires que nous renvoient nos sens, à partir de nos souvenirs,  de nos apprentissages, de nos émotions, de nos attentes, nous reconstruisons un monde cohérent et continu dans lequel nous nous inscrivons  et dans lequel nous nous projetons. Cette reconstruction, cette réinvention permanente, s’ancre dans notre expérience, dans nos échanges avec les autres, mais aussi dans un immense socle de connaissances accumulées, renouvelées et transmises à travers le temps.

« La terre tournait, écrit Marguerite Yourcenar dans « L’œuvre au noir », formant un cercle sans commencement ni fin. La chambre donnait de la bande, les sangles criaient comme des amarres, le lit glissait d’occident en orient à l’inverse du mouvement apparent du ciel. Le point de l’espace où Zénon se trouvait contiendrait une heure plus tard la mer et ses vagues ; un peu plus tard encore, les Amériques et le continent d’Asie. Ces régions où il n’irait pas se superposaient dans l’abîme, à l’hospice de Saint-Côme. Zénon, lui-même, se dissipait comme une cendre au vent ».

Nous voyons chaque matin le soleil se lever à l’est, monter dans le ciel, puis descendre vers l’ouest et se coucher, plonger sous l’horizon et disparaître alors que monte la nuit. Et nous savons que ce que nous renvoient nos sens est pour partie une illusion.

Depuis le XVème siècle, la science occidentale a redécouvert le mouvement, le changement, l’impermanence là où nos sens nous donnent l’illusion de la stabilité. La terre plate est devenue ronde.  Copernic et Galilée ont transformé notre planète immobile au centre du ciel autour de laquelle voyageaient le soleil, la lune, les étoiles, en une sphère qui tourne sur elle-même en 24 heures tout en tournant avec d’autres planètes autour du soleil. Et de ces mouvements  naissent les jours, les nuits et les saisons.

Puis Darwin transforme le petit tremblement des générations au long des espèces immuables, en une généalogie arborescente commune qui a donné naissance depuis près de 4 milliards d’années à l’ensemble de la diversité du vivant.  

Au début du XXème siècle, Wegener transforme l’immobilité des continents en une dérive sans fin, scandée par des épisodes de chocs violents de fusions et de séparations, la tectonique des plaques qui fait émerger les chaînes de montagnes, les éruptions volcaniques et les tremblements de terre.

Puis Georges Lemaître transforme l’immobilité de l’univers en une fuite  et une expansion permanentes à partir d’une explosion originelle, ou proposerons d’autres plus tard, à partir d’univers qui donnent naissance les uns aux autres.

Dans l’immensité de cet univers, Einstein fait disparaître l’indépendance entre l’espace et le temps, et au cœur de l’invisible, la mécanique quantique transforme la notion même de localisation des composants élémentaires de la matière en une probabilité de présence.

« A chaque fois, une même impression de révélation, le tissu entier se déchire et s’effondre » écrira le jeune Darwin. « A chaque fois, la même impression de surprise, de stupéfaction, une réalité à priori absurde » dira Darwin, « invraisemblable » dira Wegener, « irreprésentable » dira Heisenberg, l’un des pères de la mécanique quantique.

Nous savons que ce que nous renvoient nos sens  n’est qu’un infime reflet décalé du monde que nous habitons et qui nous a fait naître. Et depuis 150 ans, nous savons qu’il y a en nous, autour de nous, un univers immense, invisible, composé d’êtres vivants microscopiques que nous ne pouvons ni voir, ni toucher, ni entendre. Nos sens nous révèlent l’infinité des formes les plus belles et les plus merveilleuses qu’évoquait Darwin. L’extraordinaire diversité  des animaux et des plantes multicellulaires dont les premiers ancêtres ont  émergé il y a environ un milliard d’années et dont certains descendants nous ont récemment donné naissance.

Mais nos sens ne nous renvoient aucun reflet de l’immense paysage, de l’immense univers des organismes animaux et végétaux unicellulaires, de l’immense univers des bactéries et des virus. Cet univers microscopique qui a, durant  trois milliards d’années, évolué en l’absence d’animaux et de plantes multicellulaires, et qui s’est propagé jusqu’à aujourd’hui en se transformant et en colonisant presque tous les recoins de notre planète et les corps de tous les animaux et de toutes les plantes. 

De même que les télescopes nous ont révélé une dimension mystérieuse, inconnue de l’univers d’innombrables galaxies loin dans le ciel, les microscopes nous ont révélé une dimension mystérieuse, inconnue de l’univers vivant : une nébuleuse de cellules.

« Chaque corps vivant, s’émerveillait Darwin en le découvrant, est comme un micro cosmos, un microcosme, un petit univers constitué d’une multitude de d’organismes qui se reproduisent, incroyablement petits et aussi nombreux que les étoiles dans le ciel ».

« Nous sommes tous, poursuivait un siècle plus tard la biologiste Lyne Margulis, un paysage pointilliste composé de minuscules êtres vivants ».

Comme l’ensemble des êtres vivants qui nous ont donné naissance, comme l’ensemble des êtres vivants qui nous entourent, les oiseaux et les fleurs, les papillons et les arbres, les colonies de bactéries et les colonies de levures, nous sommes, chacun, constitués de cellules. Nous naissons d’une cellule unique, elle-même née de la fusion de deux cellules, et nous nous transformons en une nébuleuse cellulaire composée de plusieurs dizaines de milliers de milliards de cellules qui se transforment, qui construisent, déconstruisent et reconstruisent notre corps en permanence et font émerger notre esprit. 
A cette échelle, les formes que nous renvoient nos sens, les formes de nos corps, des corps des animaux et des plantes deviennent floues. Habitant les sols, les lacs, les rivières, les mers, habitant les corps des animaux et des plants, habitant nos corps, s’y reproduisant, y résidant, ou voyageant d’un corps à l’autre, un immense univers de micro-organismes et de bactéries tisse le réseau invisible de nos écosystèmes. Et plus petit encore, naviguant de cellules en cellules à l’intérieur desquelles il leur faut pénétrer pour se reproduire, voyagent et résident d’innombrables éléments génétiques mobiles, des virus, des rétrovirus, des transposons, qui emportent avec eux des gènes qu’ils ont empruntés à leurs hôtes et les insèrent de temps à autre dans les gènes d’autres hôtes, faisant ainsi voyager les gènes d’une branche à l’autre du buisson du vivant. 

C’est dans ce monde invisible à nos sens que se déploie la plus grande diversité, la plus grande partie de l’univers vivant, et nous sommes en permanence en interface, en contact avec cet univers immense et invisible. 

Nous ne percevons pas consciemment  la présence de cet univers invisible qui nous entoure, constitué d’innombrables micro-organismes vivants, mais nous interagissons en permanence avec lui ; et avec d’autres éléments encore,  dans l’air qui nous entoure et que nous inspirons flottent au printemps d’innombrables pollens, la poussière que nous révèle parfois soudain la lumière, le sable quand le vent se lève. Nous ne percevons consciemment cet univers que lorsqu’une modification soudaine du fonctionnement de notre corps - une fièvre, des frissons, un éternuement, des courbatures, un larmoiement, une inflammation de la peau, une douleur - nous en révèle indirectement les effets dans notre corps. 

Mais si nos sens - la vision, l’audition, l’odorat, le goût, le toucher - si notre cerveau ne distingue pas en tant que telle cette dimension immense et permanente du monde dans lequel nous sommes plongés dès notre naissance, il y a en nous une forme de perception inconsciente, une interaction permanente, une réponse continuelle à ce monde invisible.

Une partie de notre corps composé des cellules qui tapissent notre peau, notre tube digestif, nos bronches, nos muqueuses, nos surfaces d’interaction directe, d’interface avec l’extérieur et un immense archipel mouvant composé de cellules différentes qui voyagent à travers notre corps, constitue  un organe morcelé, mosaïque, mobile, qu’on appelle le système immunitaire. Et cet organe mobile, inconscient, qui se déploie  à travers notre corps, qui perçoit et répond à l’environnement intérieur et extérieur du corps, a des propriétés qui évoquent pour partie, étrangement, celles de notre système nerveux et de notre cerveau : une capacité à distinguer l’intérieur de l’extérieur - le soi du non-soi -  une  capacité à distinguer le nouveau de l’ancien, une capacité d’inscrire en nous une mémoire de nos premières rencontres avec les constituants de ce monde invisible et de répondre  différemment lors des rencontres suivantes,  une capacité d’inscrire en nous une empreinte interne qui nous modifie et nous permet de nous adapter à ce monde immense et perpétuellement changeant qui nous entoure, nous  habite  et avec lequel nous coexistons. 

De nombreux êtres de ce monde nous sont indispensables, d’autres nous menacent et risquent de nous détruire. Nous vivons en interaction permanente, en équilibre dynamique avec  l’univers  bactérien. Pas seulement parce que nous sommes en contact continuel avec lui, mais parce que, comme la plupart des êtres vivants, comme la plupart des animaux, nous en tirons des ressources vitales. Nous vivons en symbiose avec de nombreuses bactéries.

Je vous ai parlé, dans une précédente émission, des symbioses, des différentes formes de symbioses qui se sont tissées au cours de l’évolution et qui ont contribué à tisser l’évolution du vivant. Lynn Margulis a proposé, à la fin des années 1960, que les premiers ancêtres des cellules de tous les animaux et de toutes les plantes sont nés il y a environ deux milliards d’années de fusions entre des œufs bactéries et des archaebactéries. Et toutes les cellules des animaux et des plantes, toutes nos  cellules portent encore en elles aujourd’hui la trace vivante de cette symbiose : de minuscules cellules qui se reproduisent à l’intérieur de nos cellules, les mitochondries, les descendants de bactéries qui ne peuvent plus vivre à l’extérieur et qui produisent à partir de l’oxygène l’essentiel de l’énergie que consomment nos cellules. 

Ecoutons Lynn Margulis : « les religions et les mythes ont toujours été emplis de sirènes, de sphinx, de vampires, de loups-garous,  de séraphins. Des êtres imaginaires nés de la combinaison de différentes portions d’animaux. La vérité étant plus étrange que la fiction, la biologie a découvert la réalité de l’existence d’êtres vivants formés de combinaisons d’autres êtres vivants. 

Mais il y a d’autres formes de symbioses qui ne constituent pas des fusions définitives et irréversibles entre des organismes appartenant à des branches distinctes du buisson du vivant. Ces autres formes de symbioses, extrêmement fréquentes, renaissent sans cesse de génération en génération à partir d’une rencontre toujours recommencée.

Je vous avais parlé des calmars. Chez les calmars, au cours de leur développement, une interaction complexe se noue entre une partie du corps - le système immunitaire -  et des bactéries luminescentes,  présentes dans la mer, qui les pénètrent. Ces interactions vont sculpter la forme d’un organe - l’organe lumineux - qui se refermera sur la colonie bactérienne à condition qu’elle se mette à émettre de la lumière. Et ainsi, à la naissance de chaque calmar, de génération en génération, recommence cette symbiose qui permet au calmar  de posséder un organe qui émet de la lumière, dans lequel les bactéries émettent de la lumière, ce faisceau lumineux qui permet au calmar de chasser dans les ténèbres des abysses. 

Dès notre naissance, notre tube digestif est colonisé par des bactéries. A l’âge adulte, notre flore intestinale bactérienne est composée de plusieurs centaines de milliers de milliards de bactéries, un nombre dix fois supérieur au nombre de cellules qui composent notre corps. Cette flore exerce une profonde influence sur notre capacité à tirer des ressources énergétiques à partir de notre nourriture et sur la maturation et le développement de notre système immunitaire qui interagit avec ces bactéries.

Une étude publiée il y a moins d’un an dans le journal  scientifique « Nature », la plus grande étude publiée à ce jour dans ce domaine, indique que chaque personne,  en Europe du moins, héberge dans son tube digestif un écosystème constitué de plus de 150 espèces bactériennes différentes sur un total dans la population de plus de 1000. Et cette flore est en partie commune, une cinquantaine d’espèces bactériennes différentes étant présentes chez plus de 90 % de personnes qui ont été explorées dans cette étude. 

L’abondance relative des différentes espèces est variable chez différentes personnes et pourrait être variable au cours du temps en fonction de l’alimentation, de l’environnement et du fonctionnement du système immunitaire.

Que révèle aussi cette étude, c’est que l’ensemble des 150 espèces bactériennes qui composent la flore intestinale de chaque personne contient en moyenne un total de  500 000 gènes différents, c’est-à-dire un nombre de gènes 20 fois supérieur au nombre de gènes que possèdent les cellules de notre corps. Et ainsi, c’est d’un immense réseau de cellules et de gènes - les nôtres et celles de l’écosystème bactérien que nous hébergeons - que résulte l’extraction et l’absorption de nos ressources énergétiques, et notamment la conversion de sucres complexes présents dans les fruits et légumes que nous ne pouvons absorber tels quels, et la production de certaines vitamines que nous absorbons. Et il semble que des bactériophages - des virus qui infectent les bactéries -  jouent un rôle important dans leur capacité à résider dans notre tube digestif et à prédigérer nos aliments.


Depuis cinq ans, une série de travaux suggère que des modifications de la flore intestinale pourraient jouer un rôle dans le développement de l’obésité et que des réponses particulières du système immunitaire à la flore, pourraient jouer un rôle important dans le développement  des maladies inflammatoires graves de l’intestin. Les cellules qui tapissent la paroi de nos intestins et les cellules mobiles du système immunitaire qui viennent y résider sont en contact et en interaction continuels avec les bactéries de la flore intestinale, y répondant en les tolérant, sans les détruire, tout en les empêchant de traverser la paroi et de pénétrer à l’intérieur du corps, un équilibre dynamique continuel. 
 
Nous vivons en équilibre, en symbiose, durant toute notre vie avec un écosystème complexe de micro-organismes qui résident dans notre tube digestif et sur notre peau et nos muqueuses. Mais d’innombrables bactéries, parasites, virus, pénètrent à l’intérieur de notre corps par l’intermédiaire de nos surfaces de contact avec l’environnement extérieur : notre peau, nos muqueuses, notre bouche, nos bronches, notre tube digestif. Ces micro-organismes voyagent d’êtres humains à êtres humains, ou d’animaux à êtres humains, et ils peuvent aujourd’hui voyager avec nous à la vitesse des avions. Beaucoup sont utilisés dans l’espèce humaine depuis très longtemps : la tuberculose, la diphtérie, la coqueluche, la poliomyélite,  mais parfois ce sont les déséquilibres causés dans nos relations avec les animaux et les plantes, la destruction de leur habitat, la destruction  des écosystèmes qui provoque brutalement  ce qu’on appelle une rupture de la barrière des espèces, favorisant la propagation  d’une bactérie, d’un parasite, d’un virus animal à la population  humaine. 

Les virus qui ont causé la pandémie actuelle du sida sont des virus de singes qui ont commencé à se propager à travers les populations humaines, il y a environ un demi siècle. Ils causent chez les êtres humains, en l’absence de traitement anti rétroviral, un effondrement du système immunitaire, des lésions nerveuses et la mort en une dizaine d’années, en moyenne. Les virus du sida tuent encore chaque année 2 millions de personnes dans les pays pauvres en raison d’une absence d’accès aux médicaments. Mais chez la plupart des singes qu’ils  infectent depuis très longtemps, ces virus se multiplient durant toute l’existence sans causer de maladie. Les singes et ces virus vivent en état d’équilibre dynamique, de coexistence auquel les a conduits une longue cohabitation. Mais le passage soudain de ces virus de singes dans des organismes d’une autre espèce - la nôtre - qui n’y était pas jusque-là, ou peu, exposée, a conduit à un déséquilibre qui provoque la tragédie. Lorsque des singes qui font partie d’espèces non exposées à ces virus sont artificiellement ou accidentellement infectés en captivité, ils développent, eux aussi, une maladie mortelle.

L’épidémie humaine  brutale et heureusement brève du SRAS qui est partie de Chine et a traversé les océans et les continents à la vitesse des avions de ligne, a été causée par les mêmes rencontres brutales entre un virus et une espèce - la nôtre - qui n’y était jusque-là pas, ou peu, exposée. 

En Asie, des dizaines, voire des centaines d’espèces de chauve-souris sont infectées depuis longtemps par ces virus sans développer de maladies. D’autres microbes ont des trajets plus complexes. Certains parasites parcourent en permanence, en zigzag,  des branches distinctes du buisson du vivant, comme le parasite du paludisme qui voyage d’un moustique à un être humain et d’un être humain à un moustique et qui tue encore aujourd’hui près de 2 millions d’enfants dans les pays pauvres du sud. Les interactions entre les corps, le système immunitaire et les microbes qui nous envahissent sont complexes, et certaines réponses du corps, certains symptômes peuvent être, selon les cas, un moyen de lutte contre le microbe ou au contraire favoriser la persistance ou la propagation du microbe.  Distinguer entre les deux est souvent difficile, mais cela était une question importante pour le médecin : essayer de déterminer quels étaient les symptômes gênants pour la personne malade  qu’il ne voulait pas essayer d’atténuer à tout prix quand ils participaient aux défenses de l’organisme. La fièvre, par exemple, si elle n’est pas très élevée et ne présente pas de risque pour l’enfant ou la personne adulte, est souvent l’un des mécanismes de lutte contre les microbes.

Mais dans certaines espèces animales, certains parasites provoquent des réactions, des symptômes a priori étranges, qui favorisent leur propagation en zigzag à travers des organismes appartenant à différentes branches du buisson de l’évolution. 

Il y a près de 30 ans, l’évolutionniste, Richard Dawkins, avait synthétisé dans un chapitre d’un très beau livre « Le phénotype étendu », plusieurs descriptions saisissantes de ces formes de coopération forcée d’animaux - au prix de leur propre perte - à la propagation des parasites qui les infectent. Il y a ainsi un parasite qui voyage de petits poissons d’eau douce à des canards, et des canards aux petits poissons. Les poissons, lorsqu’ils sont infectés, modifient leur comportement et nagent près de la surface de l’eau augmentant ainsi la probabilité de leur ingestion par les canards, et donc, la probabilité de la propagation du parasite.

Il y a un autre parasite qui voyage de fourmis en moutons et de moutons en fourmis. La fourmi infectée, au lieu de rester comme d’habitude sur le sol, a tendance à grimper au sommet des brins d’herbes et à y rester une partie de la journée, augmentant ainsi la probabilité d’être mangée par un mouton et permettant ainsi au parasite de voyager. 27.37

Il y a, en Amérique du sud, un parasite qui voyage du corps d’une fourmi noire, qui vit dans des arbres, au corps d’un oiseau ; puis du corps de l’oiseau au corps d’une fourmi noire. 

Il y a trois ans, il a été découvert que le parasite provoque chez les fourmis infectées une coloration rouge de l’abdomen qui se met à gonfler. Les Oiseaux confondent ces fourmis avec des baies, les mangent, propageant ainsi le parasite.

Je vous ai parlé, dans une précédente émission, de la théorie de la Reine Rouge proposée, il y a trente ans, par l’évolutionniste Leigh Van Valen. Cette théorie complétait la notion de coévolution, de coadaptation développée par Darwin et la notion selon laquelle l’émergence continuelle de nouveauté et sa propagation de génération en génération ne correspond pas  à une amélioration qualitative intrinsèque des organismes, ni à une marche sur la voie triomphale d’un progrès, d’une perfection.

La métaphore de la Reine Rouge fait référence à un épisode du livre « De l’autre côté  du miroir » de Lewis Carroll, la suite d’ « Alice au pays des merveilles ». Dans cet épisode, la Reine Rouge entraîne Alice dans une course de plus en plus rapide. «Ce qu’il y avait de plus curieux dans l’aventure, dit Alice, c’est que les arbres et les autres objets qui nous entouraient ne changeaient pas du tout de place ». Quand enfin elles interrompent leur course, Alice remarque que tout est demeuré exactement comme auparavant. « Dans notre pays, dit Alice à la Reine, si l’on courait très vite pendant longtemps, on arriverait en général quelque part, ailleurs ». « Un pays bien lent, dit la Reine, ici il faut courir de toute la vitesse de ses jambes pour simplement demeurer là où l’on est ». 

La théorie de la Reine Rouge proposée par Van Valen propose que la propagation des nouveautés apparues par hasard dépend des déséquilibres permanents dans les interactions entre les organismes et les espèces qui constituent et construisent les écosystèmes : à telle nouveauté qui se propagera, répondra la propagation de telle autre nouveauté apparue par hasard, si elle permet à un organisme de poursuivre ses interactions en terme de coopération ou de combat, de symbiose ou de conflit ; si elle permet à des organismes de survivre et d’avoir des descendants, de demeurer un temps là où ils sont, de changer, de courir, en demeurant simplement là où ils sont, de devenir autre, tout en se propageant à travers le temps.

La course de la Reine Rouge, un tourbillon  de changements progressifs graduels ou soudains qui modifient les interactions, les équilibres, qui renouvellent les combats entre prédateurs et proies, les attaques, les fuites, les contre-attaques, les coopérations, les coadaptations, les symbioses.

Darwin avait consacré un livre aux extraordinaires coadaptations entre les orchidées et les insectes qui les pollinisent. Certaines orchidées ont un pétale - le labelle - qui ressemble tellement à des guêpes femelles que les  guêpes mâles viennent s’y accoupler, pollinisant ainsi les orchidées. 

La coadaptation entre les plantes à fleurs et les insectes qui les pollinisent et les oiseaux qui mangent leurs fruits et disséminent  ailleurs leurs graines. Il y a chez certaines plantes des variations multiples et complexes sur ces thèmes.

Il y a trois ans, des chercheurs on découvert que les deux substances volatiles émises dans l’atmosphère en plus grande quantité par une plante à fleurs avait des effets opposés : le nectar attirait les insectes pollinisateurs, l’autre substance les repoussait. Le mélange des deux diminuait le temps passé par l’insecte pollinisateur sur une fleur, augmentant ainsi le nombre de fleurs visitées, augmentant ainsi la probabilité de pollinisation. 

Et il y a d’autres variations encore sur le thème de ces coadaptations et de ces coopérations. Lorsqu’elles sont attaquées par de petits prédateurs - des chenilles ou des pucerons qui dévorent leurs feuilles - certaines plantes émettent des molécules qui diffusent dans l’atmosphère et qui attirent, auxquelles répondent d’autres animaux qui sont des prédateurs  de ces  prédateurs de la plante et viennent les dévorer, protégeant ainsi la plante. Interaction complexe entre coopération et combat. 

Et dans cette course permanente de la Reine Rouge, dans cette émergence continuelle de la nouveauté, et cette propagation de la nouveauté, le temps des micro-organismes avec lesquels nous coexistons, le temps des bactéries, des parasites et des virus, ne bat pas la même mesure que le temps des animaux et des plantes, que notre temps à nous.

L’un des rythmes essentiels auquel bat la survenue et la propagation de la nouveauté, des changements héréditaires, est le rythme des générations. Pour nous, le temps d’une génération est d’environ 25 ans,  un quart de siècle. Pour certaines espèces bactériennes  le temps d’une génération peut être de 30 minutes. Pour ces espèces bactériennes, une durée de 25 ans correspond à plus de 400 000 générations, et à notre échelle - à l’échelle humaine - une succession de 400 000 générations correspond à une durée de plus de 10 millions d’années, une distance probablement plus grande que celle qui nous sépare des derniers ancêtres communs aux chimpanzés, aux bonobos et aux êtres humains. Ces différents battements du rythme des changements dans des espèces vivantes qui se côtoient en permanence exerce continuellement une pression sur l’adaptation, la survie, la reproduction des organismes qui participent à ces interactions. Et le rythme des générations, la succession des générations, ne constitue pas chez les bactéries le seul mécanisme, le seul rythme de l’émergence de nouveautés héréditaires.

Il y a chez les bactéries, comme dans l’ensemble du monde vivant mais de manière plus importante et beaucoup plus rapide, des phénomènes de transfert, d’échanges horizontaux de gènes, non pas uniquement de parents à descendants, mais de voisins à voisins, de vivants à vivants, mais aussi, nous l’avons vu, de voisins morts à voisins vivants. Et c’est ainsi qu’a été découvert, pour la première fois, que l’A.D.N. était le support moléculaire de l’hérédité. Ainsi, la résistance à un antibiotique, par exemple, peut se transmettre de manière horizontale à travers des colonies de bactéries qui acquièrent ces gènes de résistance de leurs voisines, puis les transmettent à leurs descendants, de génération en génération. 

La nouveauté peut aussi émerger de manière brutale chez des bactéries en réponse à des modifications défavorables de leur environnement. Une réponse  qui a été appelée « réponse S.O.S. ». C’est une réponse épigénétique, une modification dans la façon d’utiliser certains de ces gènes en réponse à des modifications de l’environnement. La plupart des bactéries contiennent des gènes qui leur permettent de fabriquer des enzymes qui ont pour effet de découper l’ensemble de leur A.D.N. et de réarranger au hasard les morceaux dans un ordre différent.

Dans des conditions d’environnement favorable, la bactérie ne fabrique pas l’ensemble de ces enzymes, mais lorsque l’environnement devient défavorable, elle les fabrique et les utilise. Elle répond à ce changement d’environnement en plongeant dans une radicale transformation intérieure.  Et si l’une de ces bactéries survie par hasard dans  cet environnement, à ce bouleversement aléatoire de son A.D.N. devenu autre, elle transmettra à ses descendants ce nouvel A.D.N. réarrangé, ce nouvel assemblage de gènes qui permet de vivre dans cet environnement.

Et ainsi, le rythme d’émergence des nouveautés génétiques héritables à travers les générations dépend, en partie, chez les bactéries, non seulement des nouveautés héritées de leurs parents, ou de leurs voisines, mais aussi du rythme des changements de l’environnement.

L’apparition de la nouveauté chez les bactéries n’est pas que de nature génétique. Elle peut être une propriété émergente de la collectivité, elle peut naître d’interactions entre des bactéries au seing d’une colonie, sans modification des gènes. Il y a quelques mois, une étude montrait que dans des colonies de bactéries qui résistaient collectivement à un antibiotique, lorsqu’on isolait ces bactéries  et qu’on les étudiait une à une, seul 1 à quelques pour cent étaient capables individuellement de résister à l’antibiotique. Toutes les autres succombaient. Mais cette toute petite minorité de bactéries génétiquement résistantes fabriquaient et libéraient en présence de l’antibiotique une molécule qui permettait au reste de la colonie - aux 90 à 99 % de leurs voisines - de résister à l’antibiotique. Et ce qui est vrai pour la résistance aux antibiotiques - ces différentes formes d’émergence et de propagation et de partage de la nouveauté - est aussi vrai à un autre niveau en ce qui concerne  les capacités de résistance des bactéries au système immunitaire et leur capacité de pénétration, de survie et de reproduction à l’intérieur de nos corps. Face à cette extraordinaire capacité à faire apparaître la nouveauté et la diversité à un rythme sans commune mesure avec le rythme de nos générations, comment réussissons-nous, notre vie durant, à coexister et à préserver notre intégrité ?

« J’en parlerai en homme qui en fut lui-même atteint et qui  vit souffrir d’autre personnes ». C’est le livre II de « l’histoire de la guerre du Péloponnèse », la guerre entre Sparte et Athènes qu’écrit Thucydide  à Athènes, au Vème siècle avant notre ère. Il parle de l’épidémie de ce qu’il nomme « la peste », qui ravage Athènes pendant que l’armée de Sparte assiège ses murs.

« Jamais on n’avait entendu parler d’une si terrible épidémie, dit Thucydide. Il est impossible de dépeindre les ravages de ce fléau, il sévissait avec une violence irrésistible. La maladie était sans remède. Ni vœux, ni prières ne contribuaient à la guérison. Les oracles et les prédictions étaient inutiles, et les esprits étaient abattus par la violence du mal. Les temples mêmes où l’on s’était retiré étaient  emplis de morts. Mais, dit Thucydide, ceux qui avaient survécu à la maladie étaient ceux qui se montraient les plus compatissants avec les mourants  et les malades car ils étaient hors de danger. On ne retombait pas deux fois dans la maladie, ou du moins, si on y retombait, on n’en mourait pas ».

C’est l’une des premières descriptions de la protection que confère pour beaucoup de microbes une première rencontre à laquelle on a survécu. C’est l’une des premières descriptions de la mémoire immunitaire. Si l’on a survécu à une première rencontre, on sera protégé lors des rencontres suivantes. C’est ce qu’on a appelé l’immunité, du nom qui était utilisé à Rome pour décrire la protection contre les poursuites judiciaires dont bénéficiaient les sénateurs pendant leur mandat : la  protection contre une nouvelle infection par le même microbe.

En Chine, depuis très longtemps sans doute, cette notion avait été mise en pratique pour protéger de la variole. On mettait en contact les enfants avec du liquide des lésions ou en faisant inhaler une poudre qui en avait été préparée à partir de lésions de personnes en train de guérir de la variole. Le contact protégeait, mais le risque était de contracter la maladie mortelle lors de la variolisation.

Au début du XVIIIème siècle, la variolisation est arrivée en Angleterre et la famille royale la mettra en pratique. Voltaire en parle dans les années 1730. Il appelle cette procédure « l’inoculation ». « Tout prouve, dit-il, que les anglais sont plus philosophes et plus hardis que nous ». Mais la véritable révolution dans ce domaine aura lieu à la fin du XVIIIème siècle.

Edward Jenner a observé que les femmes qui traient les vaches et ont aux mains des lésions qui leur ont été transmises par les vaches, sont protégées contre la variole. Pourrait-on à partir d’une maladie d’un animal,  qui ne cause pas de maladie chez l’être humain, protéger contre une maladie mortelle humaine ?  Dans ce qui est l’acte fondateur de l’immunologie, et en même temps vu avec notre regard d’aujourd’hui un scandale éthique, Edward Jenner inocule en 1796 cette infection bénigne de la vache à un enfant de 8 ans, puis trois mois plus tard, lui inocule la variole. L’enfant est protégé,  et Jenner publie sont travail en 1798. 

La vaccination - de vaccinus : qui vient de la vache -  est née. A une époque où l’on ne connait rien de l’existence de l’univers des virus, des bactéries et des parasites, et où l’on ne connait rien de l’existence d’un système immunitaire, l’observation attentive d’une relation de causalité  révélait une capacité essentielle du corps : la mémoire. Non pas la mémoire consciente - les souvenirs - mais la mémoire d’une rencontre qui modifiait le corps, le rendait autre et lui permettait de survivre à une rencontre avec une maladie mortelle. Une mémoire croisée ouverte, une première rencontre avec une maladie animale qui semblait partager certaines caractéristiques avec la maladie humaine, protégeait contre la maladie humaine. Il n’était pas nécessaire d’avoir eu une première rencontre avec la maladie pour être protégé. 

Deux siècles plus tard, en 1980, la vaccination antivariolique allait conduire à l’annonce par l’Organisation Mondiale de la Santé, de l’élimination, pour la première fois, d’une maladie infectieuse mortelle terrifiante de la population humaine : la variole.

Entre-temps, à la fin du XIXème siècle, les découvertes de Koch et de Pasteur sur les microbes allaient permettre le développement d’autres vaccinas qui auront notamment pour effet de contribuer de manière importante à la réduction de la mortalité infantile, du moins dans les pays  riches : vaccins contre la diphtérie, la coqueluche, la rougeole, la poliomyélite, la tuberculose, mais aussi le tétanos, la rage. Mais quels étaient les mécanismes qui permettaient au corps de se protéger, qui permettaient aux vaccins d’agir ?

En 1883, Ilia Metchnikoff, en étudiant les étoiles de mer, découvre l’existence de cellules capables d’ingérer les substances qui pénètrent dans le corps, puis il découvre que ces cellules qu’il nomme des « phagocytes » - les dévoreurs de cellules - sont capables d’ingérer et de détruire des microbes. L’immunité est un système cellulaire, une affaire de cellules.

Pendant ce temps, Emil Von Behring montre que des molécules solubles dans le sang permettent de protéger un animal d’un microbe.  Le transfert du sérum d’une partie du sang d’un animal qui a guéri d’une diphtérie permet de protéger contre la diphtérie des animaux qui n’y ont jamais été exposés. Ces molécules solubles circulantes neutralisent la toxine libérée par le bacille diphtérique, toxine qui est la cause de la maladie mortelle. Ces molécules sont nommées «antitoxines», puis elles seront nommées «anticorps». Et en 1900, Paul Ehrlich propose que l’immunité est due à ces substances solubles qui circulent dans le sang, ce qu’on appellera « l’immunité humorale » dans les humeurs.

En 1908, Ilia  Metchnikoff qui a rejoint l’Institut Pasteur à Paris  et Paul Ehrlich partagent le premier prix Nobel de physiologie et de médecine pour leur découverte de l’immunologie, la science qui explore  l’immunité, la protection contre les maladies infectieuses. 

L’immunité est-elle cellulaire ou humorale ? C’est à la fois une question scientifique essentielle et une querelle patriotique franco-allemande. La réponse viendra une quarantaine d’années plus tard : le système immunitaire  - ce système qui nous permet de coexister avec l’immense écosystème invisible des bactéries, des parasites, des virus qui nous entourent, nous habitent et nous menacent - commencera à révéler ses mystères, et c’est à la découverte de ces mystères que nous partirons dans une prochaine émission.

==Invité==
Christophe Rameix

==Crédits phonographiques==

Louis Chedid
On ne dit jamais assez aux gens qu'on aime qu'on les aime
label : Atmosphériques
parution : 2010

Les Têtes Raides et Jeanne Moreau
Emma
album : L'An Demain
label : tot Ou tard
parution : 2011

Djazia Satour
Klami
parution : 2010

Leonard Cohen
Dance me to the end of love
label : CBS
parution : 1984


==Crédits bibliographiques==

=Citations=

 [La Reine Rouge entraîna Alice dans une course de plus en plus rapide]. "Ce qu’il y avait de plus curieux dans l’aventure, dit Alice, 
 c’est que les arbres et les autres objets qui nous entouraient ne changeaient pas du tout de place". [Quand enfin elles interrompent 
 leur course, Alice remarque que] « tout est demeuré exactement comme auparavant". "Dans notre pays, dit Alice, si l’on courait très 
 vite pendant longtemps, on arriverait généralement quelque part, ailleurs". "Un pays bien lent ! dit la Reine. Ici, il faut courir de 
 toute la vitesse de ses jambes pour simplement demeurer là où l’on est".
de Lewis Carroll
dans De l’autre côté du miroir

=Articles=

-Qin J, Li R, Raes J et coll. A human gut microbial gene catalogue established by metagenomic sequencing. Nature 464 : 59-65, 2010.

-Lee HH, Molla MN, Cantor CN et coll. Bacterial charity work leads to population-wide résistance. Nature 467 : 82-5, 2010.

-Chun C, Troll J, Koroleva I, Brown B et coll. Effects of colonization, luminescence, and autoinducer on host transcription during development of the squid-vibrio association. Proc Natl Acad Sci USA. 105 : 11323-8, 2008.

-Yanoviak S, Kaspari M, Dudley R et coll. Parasite-induced fruit mimicry in a tropical canopy ant. Am Nat. 171 : 536-44, 2008.

-Kessler D, Gase K, Baldwin I. Field experiments with transformed plants reveal the sense of floral scents. Science 321 : 1200-2, 2008.

-Gaskett A, Winnick C, Herberstein M. Orchid sexual deceit provokes ejaculation. Am Nat. 171 : E206-12, 2008.

-Mazmanian S K, Round J, Kasper D. A microbial symbiosis factor prevents intestinal inflammatory disease. Nature 453 : 620-5, 2008.

-Turnbaugh P et coll. An obesity-associated gut microbiome with increased capacity for energy harvest. Nature 444 : 1027-31, 2006.

=Ouvrages=

Ce qu’Alice trouva de l’autre côté du miroir
de Lewis Carroll
éditeur : Folio Junior
parution : 2009
titre_original : Through the looking-glass, and What Alice found there
parution_original : 1871
 
L’Univers bactériel
de Lynn Margulis et Dorian Sagan
éditeur : Points Seuil
parution : 1989

La guerre du Péloponnèse, Livre II
de Thucydide
éditeur : Gallimard Folio
parution : 2000
parution_original : -411
 
L’œuvre au noir (Et les carnets de notes de L’œuvre au noir) 
de Marguerite Yourcenar
éditeur : Gallimard Folio
parution : 1976
parution_original : 1968

Dans la lumière et les ombres. Darwin et le bouleversement du monde
de Jean-Claude Ameisen
éditeur : Fayard / Seuil
parution : 2008

L’Univers bactériel
de Lynn Margulis et Dorian Sagan
éditeur : points seuil
parution : 2002

Le phénotype étendu
de Richard Dawkins
éditeur : Oxford University Press
parution : 1999
titre_original : The Extended Phenotype
