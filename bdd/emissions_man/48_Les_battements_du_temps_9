==Retranscription==


==Crédits phonographiques==

- Catherine Ringer - Vivie L'Amour

- Captain Kid - We and I [Savoury Snacks Records]

- Zimpala - Adios

- Nancy Sinatra and Lee Hazlewood - Summer Wine


==Crédits bibliographiques==

=Citations=

 Ce ne sont que des indices et des suppositions,
 des indices suivis de suppositions ; et le reste
 est […] pensée et action.
 L’indice à moitié deviné, le don à moitié compris […]
TS Eliot. Four Quartets.

 Je suis ici
 Ou là, ou ailleurs. […]
 Temps passé et Temps futur
 Permettent à peine d’être conscient.
 Etre conscient, ce n’est pas être inscrit dans le temps
 Et pourtant c’est seulement à l’intérieur du temps que le moment dans le jardin des roses
 Que le moment sous la tonnelle où battait la pluie
 Que le moment dans l’église où soufflait le vent et où retombait la fumée
 Peuvent être remémorés ; enchevêtrés dans le passé et le futur.
 C’est seulement à travers le temps que peut être conquis le temps.
TS Eliot. Four Quartets.

 Le temps viendra
 où, avec allégresse,
 tu t’accueilleras toi-même, arrivant
 à ta propre porte […]
 et chacun sourira et souhaitera la bienvenue à l’autre…
 et dira, assieds-toi là. Mange.
 Tu aimeras à nouveau l’étranger que tu étais.
 Donne du vin. Donne du pain. Redonne ton cœur
 à toi-même, à l’étranger qui t’a aimé
 toute ta vie, que tu as ignoré […]
 qui te connaît par cœur […]
 Assieds-toi, Fais-toi une fête de ta vie.
Derek Walcott. "Love after Love" [in : Sea grapes].

=Articles=

-Ossandón T, Jerbi K, Vidal JR, et coll. Transient suppression of broadband gamma power
in the default-mode network is correlated with task complexity and subject performance. J
Neurosci. 2011, 31:14521-30.
 
-Dastjerdi M, Foster BL, Nasrullah S, et coll. Differential electrophysiological response
during rest, self-referential, and non-self-referential tasks in human posteromedial cortex.
Proc Natl Acad Sci U S A. 2011, 108:3023-8.
 
-Sadeghi NG, Pariyadath V, Apte S, et coll. Neural correlates of subsecond time distortion in
the middle temporal area of visual cortex. J Cogn Neurosci. 2011, 23:3829-40.
 
-Biswal BB, Mennes M, Zuo XN, et coll. Toward discovery science of human brain function. Proc Natl Acad Sci U S A. 2010, 107:4734-9.
 
-Gick B, Derrick D. Aero-tactile integration in speech perception. Nature. 2009, 462:502-4.
 
-Raichle ME. A paradigm shift in functional brain imaging. J Neurosci. 2009, 29:12729-34.
 
-Munhall KG, ten Hove MW, Brammer M, et coll. Audiovisual integration of speech in a
bistable illusion. Current Biology. 2009, 19:735-9.
 
-Wu CT, Busch NA, Fabre-Thorpe M, et coll. The temporal interplay between conscious and
unconscious perceptual streams. Current Biology. 2009, 19:2003-7.
 
-Eagleman DM. Human time perception and its illusions. Curr Opin Neurobiol. 2008, 18:131-
6.
 
-Pariyadath V, Eagleman D. The effect of predictability on subjective duration. PLoS One.
2007, 2:e1264.
 
-Schwartz O, Hsu A, Dayan P. Space and time in visual context. Nat Rev Neurosci. 2007,
8:522-35.
 
-Mason MF, Norton MI, Van Horn JD, et coll. Wandering minds: the default network and
stimulus-independent thought. Science. 2007, 315:393-5.
 
-Stetson C, Cui X, Montague PR, et coll. Motor-sensory recalibration leads to an illusory
reversal of action and sensation. Neuron. 2006, 51:651-9.
 
-Eagleman DM. Visual illusions and neurobiology. Nat Rev Neurosci. 2001, 2:920-6.

-McGurk H, MacDonald J. Hearing lips and seeing voices. Nature. 1976, 264:746-8.

-Raichle M. Un cerveau jamais au repos. Pour la Science. 2010 n°393, pp. 42-7 

=Ouvrages=

La Terre vaine et autres poèmes
de Thomas Stearns Eliot
éditeur : Points (Edition bilingue)
parution : 2006

Collected Poems 1948-1984
de Derek Walcott
éditeur : Faber &amp; Faber
parution : 1992

Incognito : The secret lives of the brain
de David Eagleman
éditeur : Pantheon Books
parution : 2011

See what I’m saying. The extraordinary powers of our five senses
de Lawrence D. Rosenblum
éditeur : WW Norton and Company
parution : 2010
