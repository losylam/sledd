==Retranscription==

Bonjour à tous, sur les épaules de Darwin, sur les épaules des géants.

Si nous sommes capables de voir plus de choses et de voir plus loin, c’est parce que nous nous tenons sur les épaules des géants. « gigaltum umeris in sitem est » dit le texte latin. Parce que nous sommes assis sur les épaules des géants. C’est au milieu du douzième siècle que Jean de Salisbury attribuera cette phrase à son maitre : Bernard de Chartres. « Bernard de Chartres, écrit Jean de Salisbury, avait l’habitude de dire que nous sommes comme des nains, assis sur les épaules des géants. Et que pour cette raison nous sommes capables de voir plus de choses et de voir plus loin qu’eux. Non pas parce que nous aurions une vue d’une particulière acuité, mais parce que nous sommes portés dans les hauteurs, que nous sommes élevés par leur taille gigantesque. »

Dans la cathédrale de Chartres, dans le transept sud, sous la rosace, il y a quatre vitraux, qui figurent chacun l’un des grands prophètes de l’ancien testament. Sous la forme d’un géant. Chacun des prophètes porte, assis sur ses épaules, de taille ordinaire, l’un des quatre évangélistes du nouveau testament. L’évangéliste Luc se tient sur les épaules du prophète Jérémie, Mathieu se tient sur les épaules d’Isaïe, Jean sur les épaules d’Ezéquiel, Marc sur les épaules de Daniel. Quatre vitraux, quatre tableaux lumineux. Comme autant de reflets, comme autant d’échos silencieux, à la phrase de Bernard de Chartres.

Quatre cent ans plus tôt, très loin de là, en Chine, au huitième siècle,  « [http://fournetmarcel.free.fr/TANG/dengguanquelou/dengguanquelou.htm Montée au pavillon des cigognes] », un poème de Wang Zhihuan, qui évoque un coucher de soleil.
 Le soleil blanc s’appuie sur la montagne et disparait
 Le fleuve jaune, pénètre dans la mer en coulant
 Si tu veux du regard embrasser mille lieux
 Monte encore un étage.

Si tu veux voir plus loin dit le poème, monte plus haut et voir plus loin c’est aussi voir plus longtemps. Avant que le soleil ne disparaisse derrière les montagnes. Voir plus de choses à la fois dans l’espace et dans le temps.

Mille ans après le poème, cinq siècles après la phrase de Bernard de Chartres, Isaac Newton reprendra à son tour la métaphore et en changera un peu le sens.

Voir plus de choses et plus loin disait Bernard de Chartres, Voir plus loin plus longtemps disait Wang Zhihuan, Voir plus précisément, plus en profondeur, mieux voir, dira Newton.
En 1776, dans une lettre adressée à Robert ROOCK, un autre grand savant anglais, NEWTON écrit : « ce que Descartes à fait était un pas important dans la bonne direction, Vous-même avez beaucoup ajouté, de nombreuses manières, quant à moi, si j’ai vu un petit peu mieux, c’est parce que je me tenais sur les épaules des géants. »

Mieux voir dit NEWTON, il ne s’agit plus de théologie, ni de poésie, il s’agit des débuts de la grande aventure de la science moderne, de la grande aventure des mathématiques et de la physique moderne. Les géants qui ont porté Newton dans les hauteurs, sont : Nicolas COPERNICK, Dick OBRAE, Johannes KEPLER, Galileo Galilei.

Écoutons Galilé, en 1623, dans « il sagia torre », « la philosophie est écrite dans ce livre gigantesque qui est en permanence ouvert devant nos yeux, je parle de l’univers, mais on ne peut pas le comprendre si l’on n'apprend pas d'abord à connaitre la langue, et les lettres et les mots dans lesquels il est écrit. Il est écrit en langage géométrique, en langage mathématique » Et un demi siècle plus tard, Isaac Newton sera le premier à exprimer en langage mathématique, l’une des grande lois de la nature : la force d’attraction universelle. 

Quels que soient  les objets en présence, les corps terrestre ou céleste, la force d’attraction universelle se révèle proportionnelle au produit de leur masse et inversement proportionnelle au carré de la distance qui les sépare. Voir au-delà des apparences, voir dans l’invisible les relations de causalité, les lois de la nature.

Plus tard d’autres viendront, qui monteront sur les épaules de newton et verront plus loin, verront mieux, verront d’autres lois plus étranges encore. Bouleversant nos représentations de l’univers, de l’espace et du temps.

Mais il arrive aussi que les géants qui nous précèdent, sombrent dans l’oubli. Que leurs œuvres se perdent et disparaissent, puis un jour ressurgissent s’ouvrant enfin dans les hauteurs. Cela s’est produit au début du 15ème siècle, en Italie, au début du Quattrocento et cela débuta par la découverte de manuscrits perdus, égarés, oubliés, durant 1500 ans de plus.

Ceux que l’on nommera les « humanistes » feront revivre les œuvres des géants de l’antiquité grecque et romaine, monteront sur leurs épaules et découvriront des continents disparus. On a appelé cette période la Renaissance.

Renaissance, écrit pascal Guignard, cet appel à renaitre, à recommencer la vie dans son impulsion même, dans sa naissance, dans sa nouveauté.

Renaissance aux yeux de Pétrarque et d'Hokuzai, aux yeux de maitre Eckhart et Jordano Bruno, ou de Montaigne ou de Shakespeare, ne voulut jamais dire restauration des anciens dans leur ancienneté, mais renaissance de la naissance même. Redécouvrir la source, revenir à la source, et se laisser emporter plus loin, ailleurs par le courant. 

Ils étaient quatre amis, ils se réunissaient régulièrement chez l’un d’entre eux, Coluccio Salutati, le chancelier de la république de florence. C’est le tout début du 15e siècle, il y a là Leonardo Bruni qui fera traduire les grands philosophes de l’antiquité grecque et qui deviendra secrétaire de plusieurs pape, puis succèdera à Salutati comme chancelier de Florence. Il y a Nicolo Nicoli, le bibliophile, qui lèguera sa riche bibliothèque à la ville de Florence et il y a Gene Francisco Pasolini, le Pogge qui sera secrétaire de plusieurs papes avant, à son tour, de devenir chancelier de la république de florence. Quatre amis qui formaient un cercle de lettrés, deviendront les premiers humanistes.  

Poggio sera l’ami d’artistes, dont le peintre et sculpteur Donatello et de grands penseurs dont Nicolas de Cuza, qui sera cardinal et qui écrira un siècle avant Copernic : « la terre ne peut être le centre de l’univers, ne peut pas ne pas être en mouvement ». Poggio était connu pour avoir écrit une série de comptes comiques  et indécents « les facéties », mais il devait sa célébrité à la découverte de manuscrit. Il dit que le poète Pétrarque était devenu célèbre, vers 1330, en découvrant des manuscrits oubliés de Cicéron, et l’histoire de rome de Titlive, l’Italie était fascinée par la recherche de manuscrits perdus, les textes découverts, étaient copiés, diffusés, commentés et formaient la base de ce que l’on appelera plus tard les Humanités.

Entre 1414 et 1418, Poggio, se rendra à de nombreuses reprises en Suisse, pour prendre part aux travaux du concile de Constance. C’est une période de conflits religieux, de combats violents, de schismes, entre les papes de rome et les papes qui ont établis leur résidence à Avignon.

Une période où émerge un mouvement de retour aux sources du christianisme, qui donnera naissance à la Réforme, au protestantisme.

En 1415, Jan Hus, Jean Huss, recteur de l’université de Prague et l’un des propagateur de l’idée de réforme, est convoqué au concile de Constance, emprisonné, jugé et brulé.

Entre 1414 et 1418, Poggio visite les abbayes de Saint-Gall, de Reicheinau, de Veingarten et il y découvre des manuscrits perdus. Des comédies de Plaute, des textes de Cicéron, l’œuvre majeure du grand rhétoricien romain, du premier siècle de notre ère, l’avocat et professeur d’éloquence Quintilien. Une œuvre qui transformera l’enseignement dans les universités à travers l’Europe. 

Quintilien parle aussi de l’éducation du petit enfant dès son plus jeune âge. Il prescrit un enseignement fondé sur un travail d’amour et non de devoirs. Il propose une éducation à l’école et non par un précepteur, Mais recommande que le nombre d’élève par classe ne dépasse pas le nombre d’élève qui permet à l’enseignant de s’occuper de chacun. Et Poggio découvre « des architecturas » le manuscrit d’un grand architecte romain du premier siècle avant notre ère, Vitruve, une œuvre qui révolutionnera l’architecture de la Renaissance.

Mais c’est en janvier 1417, écrit Stephen GRUIMBLAT, qui enseigne les humanités à l’université Harvard, c’est en janvier 1417, dans la bibliothèque d’un monastère, que le pogge fit sa plus grande découverte. Un long poème de Titus Lucressius Carus, De Natura rerum, de la nature des choses. 7400 lignes, divisées en 6 livres, écrit en hexamètres, les vers de 6 pieds non rimés, dans lesquels écrivaient Virgile et Ovide, un poème d’une intense beauté lyrique, qui mêle des méditations philosophiques sur la religion, le plaisir et la mort et des théories scientifiques sur la nature. Un sens du merveilleux et une compréhension étonnamment moderne de l’univers. 

Une soixantaine d’années après la découverte du pogge, le clergé de Florence interdira la lecture de Lucrèce. "Mais les poèmes" dit GRIMBLAT, "sont difficiles à faire taire". Il y a des moments rares et puissants ou un écrivain disparu depuis longtemps semble se tenir devant vous et vous parler directement, comme s’il tenait un message à votre intention. 

Ecoutons pascal Guignard :

 Poursuivre à partir de 14’44’’

==Crédits phonographiques==

Lisa Portelli
Les chiens dorment
album : Le régal
label : Wagram
parution : 2011

Aurelio et Youssou N'Dour
Lubara Wanwa
album : Laru Beya
label : Stonetree Rec
parution : 2011

The Ink Spot
I don't want to set the world on fire
label : Fantastic Voyage
parution : 2009

Dominique A
L'Amour
label : IFR
parution : 2007


==Crédits bibliographiques==

=Citations=

 Il ne s’agit pas de revivre : il s’agit de recommencer la vie dans son impulsion même. Dans sa naissance. Dans sa nouveauté.  
 Renaissance, aux yeux de Pétrarque ou de Cusa, aux yeux d’Eckart et de Bruno ou de Montaigne ou de Shakespeare, ne voulut jamais dire  
 restauration des Anciens dans leur ancienneté – mais renaissance de la naissance même.  
 Rallumer à l’intensité de ce qui commence, tout ce qui succède.   
 Retrouver l’aube.  
 Naître.
de Pascal Quignard
dans Réthorique spéculative

 C’est le mot si étrange de l’Empereur Claude : ‘les choses les plus anciennes ont été extrêmement neuves’.  
 Novissima. Les novissimes sont les originaires.  
 (Les choses les plus anciennes ont été les plus neuves des choses.) […]  
 Le passé le plus lointain est le plus dense de l’énergie de l’explosion.
de Pascal Quignard
dans Les ombres errantes

 Les mêmes éléments qui forment le ciel, la mer, les terres, les fleuves, le soleil forment aussi les épis, les arbres, les êtres  
 vivants…   
 Mais les mélanges, l’ordre des combinaisons, les mouvements, voilà ce qui diffère.   
 Réfléchis. Même dans les vers des poèmes, tu vois de nombreuses lettres communes à de nombreux mots. Et cependant, ces vers, ces mots, 
 est-ce qu’ils ne sont pas différents, à la fois par le sens et par le son ?   
 Tel est le pouvoir des lettres quand seulement l’ordre en est changé. 
de Lucrèce
dans De rerum natura

 Je possède 2 petits embryons fixés dans l’alcool, que j’ai oublié d’étiqueter. A présent, je suis incapable de déterminer à quel genre 
 ils appartiennent. Il pourrait s’agir de lézards, de petits oiseaux, ou même de mammifères. La formation des têtes et des troncs de 
 ces embryons est très semblable, et les membres ne sont pas encore présents.   
 Mais même s’ils étaient déjà présents, dans les premières étapes de leur développement, cela n’indiquerait rien. Car les pattes des 
 lézards et des mammifères, et les ailes et les pattes des oiseaux, de même que les bras et les jambes des êtres humains se développent 
 à partir de la même forme fondamentale. 
de Ernst von Baer
dans Histoire du développement des animaux : observations et réflexions

 Toute vraie classification est généalogique, et la communauté de descendance est le lien caché que les naturalistes ont cherché 
 inconsciemment.  
 La communauté des structures embryonnaires révèle la communauté de la descendance […] quel que soit le degré auquel la structure de 
 l’adulte a pu être modifiée et obscurcie. 
de Charles Darwin
dans De l’origine des espèces. 

 Nous errons dans des temps qui ne sont pas [les] nôtres.
de Pascal
dans Pensées

=Articles=

Stephen Greenblatt. Annals of culture. The answer man. The New Yorker, August 8, 2011, pp 28-33.

Kalinka A, Varga K, Gerrard D et coll. Gene expression divergence recapitulates the developmental hourglass model. Nature 468:811-4, 2010.
  
Domazet-Loso T, Tautz D. A phylogenetically based transcriptome age index mirrors ontogenetic divergence patterns. Nature 468:815-8, 2010. 
 
Prud’Homme B, Compel N. Genomic hourglass. Nature 468:768-9, 2010. 
 
Irie N, Kuratani S. Comparative transcriptome analysis reveals vertebrate phylotypic period during organogenesis. Nature Communications doi :10.1038/ncomms1248, 2011.

=Ouvrages=

De la nature des choses
de Lucrèce
éditeur : Garnier Flammarion
parution : 1999

Dans la lumière et les ombres. Darwin et le bouleversement du monde
de Jean-Claude Ameisen
éditeur : Fayard / Seuil
parution : 2008

La terre, des mythes au savoir
de Hubert Krivine
éditeur : Cassini
parution : 2011

Developmental Biology, 8th edition
de Scott Gilbert
éditeur : Sinauer Associates
parution : 2006

Réthorique spéculative
de Pascal Quignard
éditeur : Folio
parution : 1995

Les Ombres errantes : Tome 1, Dernier royaume
de Pascal Quignard
éditeur : Gallimard collection Folio
parution : 2002
